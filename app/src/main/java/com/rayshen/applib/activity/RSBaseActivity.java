package com.rayshen.applib.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rayshen.applib.event.BusManager;

/**
 * Created by Shu-Hua Shen on 2017/10/3.
 */
public abstract class RSBaseActivity extends AppCompatActivity implements View.OnClickListener {

    protected Context mContext;

    protected abstract void initView();
    protected abstract void initData();
    protected abstract void bindEvent();
    protected abstract void processClick(View view);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        if (isRegisterEvent()) {
            BusManager.getBus().register(this);
        }
        RSActivityManager.getInstance().addActivity(this);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        initData();
        initView();
        bindEvent();
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        initData();
        initView();
        bindEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isRegisterEvent()) {
            BusManager.getBus().unregister(this);
        }
        RSActivityManager.getInstance().removeActivity(this);
    }

    @Override
    public void onClick(View v) {
        processClick(v);
    }

    protected <E extends View> E bind(@IdRes int viewId) {
        return (E) super.findViewById(viewId);
    }

    protected <E extends View> E bind(@NonNull View view, @IdRes int viewId) {
        return (E) view.findViewById(viewId);
    }

    protected <E extends View> void setOnClickListener(@NonNull E view) {
        view.setOnClickListener(this);
    }

    protected boolean isRegisterEvent() {
        return false;
    }
}
