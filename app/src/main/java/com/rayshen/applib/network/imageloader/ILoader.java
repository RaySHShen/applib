package com.rayshen.applib.network.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.RequestBuilder;
import com.rayshen.applib.network.config.GlobalConfig;

import java.io.File;

/**
 * Created by Shu-Hua Shen on 2017/8/12.
 */
public interface ILoader {

    void init(Context context);

    void loadNet(ImageView target, String url, Options options);

    void loadResource(ImageView target, int resId, Options options);

    void loadAssets(ImageView target, String assetName, Options options);

    void loadFile(ImageView target, File file, Options options);

    void loadNetCenterCrop(ImageView target, String url, Options options);

    void loadResourceCenterCrop(ImageView target, int resId, Options options);

    void loadAssetsCenterCrop(ImageView target, String assetName, Options options);

    void loadFileCenterCrop(ImageView target, File file, Options options);

    void loadNetFitCenter(ImageView target, String url, Options options);

    void loadResourceFitCenter(ImageView target, int resId, Options options);

    void loadAssetsFitCenter(ImageView target, String assetName, Options options);

    void loadFileFitCenter(ImageView target, File file, Options options);

    void loadNetCenterInside(ImageView target, String url, Options options);

    void loadResourceCenterInside(ImageView target, int resId, Options options);

    void loadAssetsCenterInside(ImageView target, String assetName, Options options);

    void loadFileCenterInside(ImageView target, File file, Options options);

    void loadNetCircleCrop(ImageView target, String url, Options options);

    void loadResourceCircleCrop(ImageView target, int resId, Options options);

    void loadAssetsCircleCrop(ImageView target, String assetName, Options options);

    void loadFileCircleCrop(ImageView target, File file, Options options);

    void clearMemoryCache(Context context);

    void clearDiskCache(Context context);

    void resumeRequests(Context context);

    void pauseRequests(Context context);

    RequestBuilder<File> downloadOnly(Context context);

    RequestBuilder<File> downloadOnly(Context context, Object model);

    class Options {

        public static final int RES_NONE = -1;
        public int loadingResId;
        public int loadErrorResId;

        public static Options defaultOptions() {
            return new Options(GlobalConfig.IL_LOADING_RES, GlobalConfig.IL_ERROR_RES);
        }

        public Options(int loadingResId, int loadErrorResId) {
            this.loadingResId = loadingResId;
            this.loadErrorResId = loadErrorResId;
        }
    }
}
