package com.rayshen.applib.network.http.subscriber;

import com.rayshen.applib.network.http.callback.AsyncCallback;
import com.rayshen.applib.network.http.exception.ApiException;
import com.rayshen.applib.network.http.subscriber.base.ApiBaseSubscriber;

/**
 * Created by Shu-Hua Shen on 2017/9/2.
 */
public class ApiCallbackSubscriber<T> extends ApiBaseSubscriber<T> {

    protected AsyncCallback<T> callBack;
    protected T data;

    public ApiCallbackSubscriber(AsyncCallback<T> callBack) {
        if (callBack == null) {
            throw new NullPointerException("this callback is null!");
        }
        this.callBack = callBack;
    }

    public T getData() {
        return data;
    }

    @Override
    protected void onError(ApiException e) {
        if (e == null) {
            callBack.onFail(-1, "This ApiException is Null.");
            return;
        }
        callBack.onFail(e.getCode(), e.getMessage());
    }

    @Override
    public void onNext(T t) {
        this.data = t;
        callBack.onSuccess(t);
    }

    @Override
    public void onComplete() {

    }
}
