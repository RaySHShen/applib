package com.rayshen.applib.network.imageloader;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.LongSparseArray;
import android.webkit.MimeTypeMap;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by Ray Shen on 2018/4/18.
 */
public class RxImageDownloader
{
    private static volatile RxImageDownloader sRxImageDownloader;

    private Context mContext;
    private DownloadManager mDownloadManager;

    private LongSparseArray<PublishSubject<String>> mSubjectMap = new LongSparseArray<>();

    private DownloadStatusReceiver mDownloadStatusReceiver;

    public static RxImageDownloader getInstance(Context context) {
        if (sRxImageDownloader == null) {
            synchronized (RxImageDownloader.class) {
                if (sRxImageDownloader == null) {
                    sRxImageDownloader = new RxImageDownloader(context);
                }
            }
        }
        return sRxImageDownloader;
    }

    private RxImageDownloader(@NonNull Context context) {
        this.mContext = context;
        mDownloadStatusReceiver = new DownloadStatusReceiver();
        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        context.registerReceiver(mDownloadStatusReceiver, intentFilter);
    }

    private DownloadManager.Request createRequest(@NonNull String url,
            @NonNull String filename,
            @Nullable String destinationPath,
            boolean inPublicDir,
            boolean showCompletedNotification) {

        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(filename);
        request.setMimeType(mimeTypeMap.getMimeTypeFromExtension(url));

        if (destinationPath == null) {
            destinationPath = Environment.DIRECTORY_DOWNLOADS;
        }

        File destinationFolder = inPublicDir
                ? Environment.getExternalStoragePublicDirectory(destinationPath)
                : new File(mContext.getFilesDir(), destinationPath);

        createFolderIfNeeded(destinationFolder);
        removeDuplicateFileIfExist(destinationFolder, filename);

        if (inPublicDir) {
            request.setDestinationInExternalPublicDir(destinationPath, filename);
        } else {
            request.setDestinationInExternalFilesDir(mContext, destinationPath, filename);
        }

        request.allowScanningByMediaScanner();

        request.setNotificationVisibility(showCompletedNotification
                ? DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED
                : DownloadManager.Request.VISIBILITY_VISIBLE);

        return request;
    }

    public Observable<String> download(@NonNull String url,
            @NonNull String filename,
            @NonNull String destinationPath,
            boolean showCompletedNotification) {
        return download(createRequest(url, filename, destinationPath,
                true, showCompletedNotification));
    }

    public Observable<String> downloadInFilesDir(@NonNull String url,
            @NonNull String filename,
            @NonNull String destinationPath,
            boolean showCompletedNotification) {
        return download(createRequest(url, filename, destinationPath,
                false, showCompletedNotification));
    }

    public Observable<String> download(DownloadManager.Request request) {
        long downloadId = getDownloadManager().enqueue(request);

        PublishSubject<String> publishSubject = PublishSubject.create();
        mSubjectMap.put(downloadId, publishSubject);

        return publishSubject;
    }

    private void createFolderIfNeeded(@NonNull File folder) {
        if (!folder.exists() && !folder.mkdirs()) {
            throw new RuntimeException("Can't create directory");
        }
    }

    private void removeDuplicateFileIfExist(@NonNull File folder, @NonNull String fileName) {
        File file = new File(folder, fileName);
        if (file.exists() && !file.delete()) {
            throw new RuntimeException("Can't delete file");
        }
    }

    private class DownloadStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
            final PublishSubject<String> publishSubject = mSubjectMap.get(id);

            if (publishSubject == null) {
                return;
            }

            DownloadManager.Query query = new DownloadManager.Query();
            query.setFilterById(id);
            DownloadManager downloadManager = getDownloadManager();
            Cursor          cursor          = downloadManager.query(query);

            if (!cursor.moveToFirst()) {
                cursor.close();
                downloadManager.remove(id);
                publishSubject.onError(new IllegalStateException("Cursor empty, this shouldn't happened"));
                mSubjectMap.remove(id);
                return;
            }

            int statusIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);

            if (DownloadManager.STATUS_SUCCESSFUL != cursor.getInt(statusIndex)) {
                cursor.close();
                downloadManager.remove(id);
                publishSubject.onError(new IllegalStateException("Download Failed"));
                mSubjectMap.remove(id);
                return;
            }

            int uriIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
            String downloadedPackageUriString = cursor.getString(uriIndex);
            cursor.close();

            publishSubject.onNext(downloadedPackageUriString);
            publishSubject.onComplete();

            mSubjectMap.remove(id);

            if (mDownloadStatusReceiver != null) {
                mContext.unregisterReceiver(mDownloadStatusReceiver);
            }
        }
    }

    @NonNull
    private DownloadManager getDownloadManager() {
        if (mDownloadManager == null) {
            mDownloadManager = (DownloadManager) mContext.getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
        }
        if (mDownloadManager == null) {
            throw new RuntimeException("Can't get DownloadManager from system service");
        }
        return mDownloadManager;
    }
}
