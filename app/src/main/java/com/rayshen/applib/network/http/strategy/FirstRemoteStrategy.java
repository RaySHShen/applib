package com.rayshen.applib.network.http.strategy;

import com.rayshen.applib.network.http.core.ApiCache;
import com.rayshen.applib.network.http.strategy.base.RSBaseCacheStrategy;
import com.rayshen.applib.network.mode.CacheResult;

import java.lang.reflect.Type;
import java.util.Arrays;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/**
 * Created by Shu-Hua Shen on 2017/9/5.
 */
public class FirstRemoteStrategy<T> extends RSBaseCacheStrategy<T> {
    @Override
    public <T> Observable<CacheResult<T>> execute(ApiCache apiCache, String cacheKey, Observable<T> source, Type type) {
        Observable<CacheResult<T>> remote = loadRemote(apiCache, cacheKey, source);
        remote.onErrorReturn(new Function<Throwable, CacheResult<T>>() {
            @Override
            public CacheResult<T> apply(Throwable throwable) throws Exception {
                return null;
            }
        });
        Observable<CacheResult<T>> cache = loadCache(apiCache, cacheKey, type);
        return Observable.concatDelayError(Arrays.asList(remote,cache)).filter(new Predicate<CacheResult<T>>() {
            @Override
            public boolean test(CacheResult<T> tCacheResult) throws Exception {
                return tCacheResult != null && tCacheResult.getCacheData() != null;
            }
        }).firstElement().toObservable();
    }
}
