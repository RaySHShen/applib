package com.rayshen.applib.network.http.config;

import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.network.http.core.ApiCookie;
import com.rayshen.applib.network.http.interceptor.GzipRequestInterceptor;
import com.rayshen.applib.network.http.interceptor.OfflineCacheInterceptor;
import com.rayshen.applib.network.http.interceptor.OnlineCacheInterceptor;
import com.rayshen.applib.network.mode.ApiHost;

import java.io.File;
import java.net.Proxy;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

import okhttp3.Cache;
import okhttp3.ConnectionPool;
import okhttp3.Call.Factory;
import okhttp3.Interceptor;

import retrofit2.CallAdapter;
import retrofit2.Converter;

/**
 * Created by Shu-Hua Shen on 2018/8/22.
 */
public class RSHttpGlobalConfig {

    private CallAdapter.Factory callAdapterFactory;
    private Converter.Factory   converterFactory;
    private Factory             callFactory;
    private SSLSocketFactory    sslSocketFactory;
    private HostnameVerifier    hostnameVerifier;
    private ConnectionPool      connectionPool;
    private Map<String, String> globalParams  = new LinkedHashMap<>();
    private Map<String, String> globalHeaders = new LinkedHashMap<>();
    private boolean   isHttpCache;
    private File      httpCacheDirectory;
    private Cache     httpCache;
    private boolean   isCookie;
    private ApiCookie apiCookie;
    private String    baseUrl;
    private int       retryDelayMillis;
    private int       retryCount;

    private static RSHttpGlobalConfig instance;

    private RSHttpGlobalConfig() {
    }

    public static RSHttpGlobalConfig getInstance() {
        if (instance == null) {
            synchronized (RSHttpGlobalConfig.class) {
                if (instance == null) {
                    instance = new RSHttpGlobalConfig();
                }
            }
        }
        return instance;
    }

    /**
     * @param factory
     * @return
     */
    public RSHttpGlobalConfig callAdapterFactory(CallAdapter.Factory factory) {
        this.callAdapterFactory = factory;
        return this;
    }

    /**
     * @param factory
     * @return
     */
    public RSHttpGlobalConfig converterFactory(Converter.Factory factory) {
        this.converterFactory = factory;
        return this;
    }

    /**
     * @param factory
     * @return
     */
    public RSHttpGlobalConfig callFactory(Factory factory) {
        this.callFactory = checkNotNull(factory, "factory == null");
        return this;
    }

    /**
     *
     * @param sslSocketFactory
     * @return
     */
    public RSHttpGlobalConfig SSLSocketFactory(SSLSocketFactory sslSocketFactory) {
        this.sslSocketFactory = sslSocketFactory;
        return this;
    }

    /**
     *
     * @param hostnameVerifier
     * @return
     */
    public RSHttpGlobalConfig hostNameVerifier(HostnameVerifier hostnameVerifier) {
        this.hostnameVerifier = hostnameVerifier;
        return this;
    }

    /**
     *
     * @param connectionPool
     * @return
     */
    public RSHttpGlobalConfig connectionPool(ConnectionPool connectionPool) {
        this.connectionPool = checkNotNull(connectionPool, "connectionPool == null");
        return this;
    }

    /**
     *
     * @param globalHeaders
     * @return
     */
    public RSHttpGlobalConfig globalHeaders(Map<String, String> globalHeaders) {
        if (globalHeaders != null) {
            this.globalHeaders = globalHeaders;
        }
        return this;
    }

    /**
     *
     * @param globalParams
     * @return
     */
    public RSHttpGlobalConfig globalParams(Map<String, String> globalParams) {
        if (globalParams != null) {
            this.globalParams = globalParams;
        }
        return this;
    }

    /**
     *
     * @param isHttpCache
     * @return
     */
    public RSHttpGlobalConfig setHttpCache(boolean isHttpCache) {
        this.isHttpCache = isHttpCache;
        return this;
    }

    /**
     *
     * @param httpCacheDirectory
     * @return
     */
    public RSHttpGlobalConfig setHttpCacheDirectory(File httpCacheDirectory) {
        this.httpCacheDirectory = httpCacheDirectory;
        return this;
    }

    /**
     *
     * @param httpCache
     * @return
     */
    public RSHttpGlobalConfig httpCache(Cache httpCache) {
        this.httpCache = httpCache;
        return this;
    }

    /**
     *
     * @param isCookie
     * @return
     */
    public RSHttpGlobalConfig setCookie(boolean isCookie) {
        this.isCookie = isCookie;
        return this;
    }

    /**
     *
     * @param cookie
     * @return
     */
    public RSHttpGlobalConfig apiCookie(ApiCookie cookie) {
        this.apiCookie = checkNotNull(cookie, "cookieManager == null");
        return this;
    }

    /**
     *
     * @param baseUrl
     * @return
     */
    public RSHttpGlobalConfig setBaseUrl(String baseUrl) {
        this.baseUrl = checkNotNull(baseUrl, "baseUrl == null");
        ApiHost.setHost(this.baseUrl);
        return this;
    }

    /**
     *
     * @param retryDelayMillis
     * @return
     */
    public RSHttpGlobalConfig retryDelayMillis(int retryDelayMillis) {
        this.retryDelayMillis = retryDelayMillis;
        return this;
    }

    /**
     *
     * @param retryCount
     * @return
     */
    public RSHttpGlobalConfig retryCount(int retryCount) {
        this.retryCount = retryCount;
        return this;
    }

    /**
     *
     * @param proxy
     * @return
     */
    public RSHttpGlobalConfig proxy(Proxy proxy) {
        RxHttp.getOkHttpBuilder().proxy(checkNotNull(proxy, "proxy == null"));
        return this;
    }

    /**
     *
     * @param timeout
     * @return
     */
    public RSHttpGlobalConfig connectTimeout(int timeout) {
        return connectTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     *
     * @param timeout
     * @return
     */
    public RSHttpGlobalConfig readTimeout(int timeout) {
        return readTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     *
     * @param timeout
     * @return
     */
    public RSHttpGlobalConfig writeTimeout(int timeout) {
        return writeTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     *
     * @param timeout
     * @param unit
     * @return
     */
    public RSHttpGlobalConfig connectTimeout(int timeout, TimeUnit unit) {
        if (timeout > -1) {
            RxHttp.getOkHttpBuilder().connectTimeout(timeout, unit);
        } else {
            RxHttp.getOkHttpBuilder().connectTimeout(CommonConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        }
        return this;
    }

    /**
     *
     * @param timeout
     * @param unit
     * @return
     */
    public RSHttpGlobalConfig writeTimeout(int timeout, TimeUnit unit) {
        if (timeout > -1) {
            RxHttp.getOkHttpBuilder().writeTimeout(timeout, unit);
        } else {
            RxHttp.getOkHttpBuilder().writeTimeout(CommonConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        }
        return this;
    }

    /**
     *
     * @param timeout
     * @param unit
     * @return
     */
    public RSHttpGlobalConfig readTimeout(int timeout, TimeUnit unit) {
        if (timeout > -1) {
            RxHttp.getOkHttpBuilder().readTimeout(timeout, unit);
        } else {
            RxHttp.getOkHttpBuilder().readTimeout(CommonConfig.DEFAULT_TIMEOUT, TimeUnit.SECONDS);
        }
        return this;
    }

    /**
     *
     * @param interceptor
     * @return
     */
    public RSHttpGlobalConfig interceptor(Interceptor interceptor) {
        RxHttp.getOkHttpBuilder().addInterceptor(checkNotNull(interceptor, "interceptor == null"));
        return this;
    }

    /**
     * network interceptor
     *
     * @param interceptor
     * @return
     */
    public RSHttpGlobalConfig networkInterceptor(Interceptor interceptor) {
        RxHttp.getOkHttpBuilder().addNetworkInterceptor(checkNotNull(interceptor, "interceptor == null"));
        return this;
    }

    /**
     * post body use gzip or not, if server is not support, then don't set this
     *
     * @return
     */
    public RSHttpGlobalConfig postGzipInterceptor() {
        interceptor(new GzipRequestInterceptor());
        return this;
    }

    /**
     *
     * @param httpCache
     * @return
     */
    public RSHttpGlobalConfig cacheOnline(Cache httpCache) {
        networkInterceptor(new OnlineCacheInterceptor());
        this.httpCache = httpCache;
        return this;
    }

    /**
     *
     * @param httpCache
     * @param cacheControlValue
     * @return
     */
    public RSHttpGlobalConfig cacheOnline(Cache httpCache, final int cacheControlValue) {
        networkInterceptor(new OnlineCacheInterceptor(cacheControlValue));
        this.httpCache = httpCache;
        return this;
    }

    /**
     *
     * @param httpCache
     * @return
     */
    public RSHttpGlobalConfig cacheOffline(Cache httpCache) {
        networkInterceptor(new OfflineCacheInterceptor(RxHttp.getContext()));
        interceptor(new OfflineCacheInterceptor(RxHttp.getContext()));
        this.httpCache = httpCache;
        return this;
    }

    /**
     *
     * @param httpCache
     * @param cacheControlValue
     * @return
     */
    public RSHttpGlobalConfig cacheOffline(Cache httpCache, final int cacheControlValue) {
        networkInterceptor(new OfflineCacheInterceptor(RxHttp.getContext(), cacheControlValue));
        interceptor(new OfflineCacheInterceptor(RxHttp.getContext(), cacheControlValue));
        this.httpCache = httpCache;
        return this;
    }

    public CallAdapter.Factory getCallAdapterFactory() {
        return callAdapterFactory;
    }

    public Converter.Factory getConverterFactory() {
        return converterFactory;
    }

    public Factory getCallFactory() {
        return callFactory;
    }

    public SSLSocketFactory getSslSocketFactory() {
        return sslSocketFactory;
    }

    public HostnameVerifier getHostnameVerifier() {
        return hostnameVerifier;
    }

    public ConnectionPool getConnectionPool() {
        return connectionPool;
    }

    public Map<String, String> getGlobalParams() {
        return globalParams;
    }

    public Map<String, String> getGlobalHeaders() {
        return globalHeaders;
    }

    public boolean isHttpCache() {
        return isHttpCache;
    }

    public boolean isCookie() {
        return isCookie;
    }

    public ApiCookie getApiCookie() {
        return apiCookie;
    }

    public Cache getHttpCache() {
        return httpCache;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public int getRetryDelayMillis() {
        if (retryDelayMillis <= 0) {
            retryDelayMillis = CommonConfig.DEFAULT_RETRY_DELAY_MILLIS;
        }
        return retryDelayMillis;
    }

    public int getRetryCount() {
        if (retryCount <= 0) {
            retryCount = CommonConfig.DEFAULT_RETRY_COUNT;
        }
        return retryCount;
    }

    public File getHttpCacheDirectory() {
        return httpCacheDirectory;
    }

    private <T> T checkNotNull(T t, String message) {
        if (t == null) {
            throw new NullPointerException(message);
        }
        return t;
    }
}
