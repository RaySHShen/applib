package com.rayshen.applib.network.http.interceptor;

import android.text.TextUtils;

import com.rayshen.applib.log.RSLog;
import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.network.RxHttp;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by Shu-Hua Shen on 2017/8/28.
 */
public class OnlineCacheInterceptor implements Interceptor {

    private String cacheControlValue;

    public OnlineCacheInterceptor() {
        this(CommonConfig.MAX_AGE_ONLINE);
    }

    public OnlineCacheInterceptor(int cacheControlValue) {
        this.cacheControlValue = String.format("max-age=%d", cacheControlValue);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
        String cacheControl = originalResponse.header("Cache-Control");
        if (TextUtils.isEmpty(cacheControl) || cacheControl.contains("no-store") || cacheControl.contains("no-cache") || cacheControl
                .contains("must-revalidate") || cacheControl.contains("max-age") || cacheControl.contains("max-stale")) {
            RSLog.d(RxHttp.TAG, "response headers : " + originalResponse.headers());
            return originalResponse.newBuilder()
                                   .header("Cache-Control", "public, " + cacheControlValue)
                                   .removeHeader("Pragma")
                                   .build();

        } else {
            return originalResponse;
        }
    }
}
