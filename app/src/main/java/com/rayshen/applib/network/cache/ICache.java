package com.rayshen.applib.network.cache;

/**
 * Created by Shu-Hua Shen on 2017/8/7.
 */
public interface ICache {
    void put(String key, Object value);
    Object get(String key);
    boolean contains(String key);
    void remove(String key);
    void clear();
}
