package com.rayshen.applib.network.http.interceptor;

import com.rayshen.applib.network.http.callback.UploadCallback;
import com.rayshen.applib.network.http.request.RSUploadProgressRequestBody;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shu-Hua Shen on 2017/8/29.
 */
public class UploadProgressInterceptor implements Interceptor {

    private UploadCallback callback;

    public UploadProgressInterceptor(UploadCallback callback) {
        this.callback = callback;
        if (callback == null) {
            throw new NullPointerException("this callback must not null.");
        }
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest = chain.request();
        if (originalRequest.body() == null) {
            return chain.proceed(originalRequest);
        }
        Request progressRequest = originalRequest.newBuilder()
                                                 .method(originalRequest.method(),
                                                         new RSUploadProgressRequestBody(originalRequest.body(), callback))
                                                 .build();
        return chain.proceed(progressRequest);
    }
}
