package com.rayshen.applib.network.http.subscriber;

import com.rayshen.applib.network.http.callback.AsyncCallback;

/**
 * Created by Shu-Hua Shen on 2017/9/2.
 */
public class DownloadCallbackSubscriber<T> extends ApiCallbackSubscriber<T> {

    public DownloadCallbackSubscriber(AsyncCallback<T> callBack) {
        super(callBack);
    }

    @Override
    public void onComplete() {
        super.onComplete();
        callBack.onSuccess(super.data);
    }
}
