package com.rayshen.applib.network.http.request;

import com.rayshen.applib.network.http.request.base.RSBaseRequest;

/**
 * Created by Shu-Hua Shen on 2017/9/9.
 */
public class RSRetrofitRequest extends RSBaseRequest<RSRetrofitRequest> {

    public RSRetrofitRequest() {

    }

    public <T> T create(Class<T> clz) {
        generateGlobalConfig();
        generateLocalConfig();
        return retrofit.create(clz);
    }
}
