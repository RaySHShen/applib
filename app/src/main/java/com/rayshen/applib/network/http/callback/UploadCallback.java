package com.rayshen.applib.network.http.callback;

/**
 * Created by Shu-Hua Shen on 2017/8/21.
 */
public interface UploadCallback {
    void onProgress(long currentLength, long totalLength, float percent);
    void onFail(int errCode, String errMsg);
}
