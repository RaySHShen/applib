package com.rayshen.applib.network.http.request.base;

import android.text.TextUtils;

import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.network.http.callback.AsyncCallback;
import com.rayshen.applib.network.http.func.RSApiFunc;
import com.rayshen.applib.network.http.func.RSApiRetryFunc;
import com.rayshen.applib.network.http.retrofit.ApiService;
import com.rayshen.applib.network.mode.ApiHost;
import com.rayshen.applib.network.mode.CacheMode;
import com.rayshen.applib.network.mode.CacheResult;

import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by Shu-Hua Shen on 2017/9/11.
 */
public abstract class RSBaseHttpRequest<R extends RSBaseHttpRequest> extends RSBaseRequest<R> {

    protected ApiService apiService;
    protected String suffixUrl = "";
    /**
     * network retry delay time (ms)
     */
    protected int       retryDelayMillis;
    /**
     * retry times
     */
    protected int       retryCount;
    protected boolean   isLocalCache;
    protected CacheMode cacheMode;
    protected String    cacheKey;
    protected long      cacheTime;
    protected Map<String, String> params = new LinkedHashMap<>();

    /**
     * Abstract method
     */
    protected abstract <T> Observable<T> execute(Type type);
    protected abstract <T> Observable<CacheResult<T>> cacheExecute(Type type);
    protected abstract <T> void execute(AsyncCallback<T> callback);

    /**
     * Constructor
     */
    public RSBaseHttpRequest() {
    }

    public RSBaseHttpRequest(String suffixUrl) {
        if (!TextUtils.isEmpty(suffixUrl)) {
            this.suffixUrl = suffixUrl;
        }
    }

    public <T> Observable<T> request(Type type) {
        generateGlobalConfig();
        generateLocalConfig();
        return execute(type);
    }

    public <T> Observable<CacheResult<T>> cacheRequest(Type type) {
        generateGlobalConfig();
        generateLocalConfig();
        return cacheExecute(type);
    }

    public <T> void request(AsyncCallback<T> callback) {
        generateGlobalConfig();
        generateLocalConfig();
        execute(callback);
    }

    protected <T> ObservableTransformer<ResponseBody, T> norTransformer(final Type type) {
        return new ObservableTransformer<ResponseBody, T>() {
            @Override
            public ObservableSource<T> apply(Observable<ResponseBody> apiResultObservable) {
                return apiResultObservable
                        .subscribeOn(Schedulers.io())
                        .unsubscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new RSApiFunc<T>(type))
                        .retryWhen(new RSApiRetryFunc(retryCount, retryDelayMillis));
            }
        };
    }

    /**
     *
     * @param paramKey
     * @param paramValue
     * @return
     */
    public R addParam(String paramKey, String paramValue) {
        if (paramKey != null && paramValue != null) {
            this.params.put(paramKey, paramValue);
        }
        return (R) this;
    }

    /**
     *
     * @param params
     * @return
     */
    public R addParams(Map<String, String> params) {
        if (params != null) {
            this.params.putAll(params);
        }
        return (R) this;
    }

    /**
     *
     * @param paramKey
     * @return
     */
    public R removeParam(String paramKey) {
        if (paramKey != null) {
            this.params.remove(paramKey);
        }
        return (R) this;
    }

    /**
     * params settins
     *
     * @param params
     * @return
     */
    public R params(Map<String, String> params) {
        if (params != null) {
            this.params = params;
        }
        return (R) this;
    }

    /**
     * retry delay time settings（ms）
     *
     * @param retryDelayMillis
     * @return
     */
    public R retryDelayMillis(int retryDelayMillis) {
        this.retryDelayMillis = retryDelayMillis;
        return (R) this;
    }

    /**
     * retry times settings
     *
     * @param retryCount
     * @return
     */
    public R retryCount(int retryCount) {
        this.retryCount = retryCount;
        return (R) this;
    }

    /**
     * use local cache or not settings
     *
     * @param isLocalCache
     * @return
     */
    public R setLocalCache(boolean isLocalCache) {
        this.isLocalCache = isLocalCache;
        return (R) this;
    }

    /**
     * cache mode settings
     *
     * @param cacheMode
     * @return
     */
    public R cacheMode(CacheMode cacheMode) {
        this.cacheMode = cacheMode;
        return (R) this;
    }

    /**
     * cache key settings
     *
     * @param cacheKey
     * @return
     */
    public R cacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
        return (R) this;
    }

    /**
     * cache time settings (ms)，default no expired
     *
     * @param cacheTime
     * @return
     */
    public R cacheTime(long cacheTime) {
        this.cacheTime = cacheTime;
        return (R) this;
    }

    public String getSuffixUrl() {
        return suffixUrl;
    }

    public int getRetryDelayMillis() {
        return retryDelayMillis;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public boolean isLocalCache() {
        return isLocalCache;
    }

    public CacheMode getCacheMode() {
        return cacheMode;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public long getCacheTime() {
        return cacheTime;
    }

    public Map<String, String> getParams() {
        return params;
    }

    @Override
    protected void generateLocalConfig() {
        super.generateLocalConfig();
        if (httpGlobalConfig.getGlobalParams() != null) {
            params.putAll(httpGlobalConfig.getGlobalParams());
        }
        if (retryCount <= 0) {
            retryCount = httpGlobalConfig.getRetryCount();
        }
        if (retryDelayMillis <= 0) {
            retryDelayMillis = httpGlobalConfig.getRetryDelayMillis();
        }
        if (isLocalCache) {
            if (cacheKey != null) {
                RxHttp.getApiCacheBuilder().cacheKey(cacheKey);
            } else {
                RxHttp.getApiCacheBuilder().cacheKey(ApiHost.getHost());
            }
            if (cacheTime > 0) {
                RxHttp.getApiCacheBuilder().cacheTime(cacheTime);
            } else {
                RxHttp.getApiCacheBuilder().cacheTime(CommonConfig.CACHE_NEVER_EXPIRE);
            }
        }
        if (baseUrl != null && isLocalCache && cacheKey == null) {
            RxHttp.getApiCacheBuilder().cacheKey(baseUrl);
        }
        apiService = retrofit.create(ApiService.class);
    }

}
