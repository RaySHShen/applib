package com.rayshen.applib.network.http.request;

import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.network.http.callback.AsyncCallback;
import com.rayshen.applib.network.http.core.ApiManager;
import com.rayshen.applib.network.http.request.base.RSBaseHttpRequest;
import com.rayshen.applib.network.http.subscriber.ApiCallbackSubscriber;
import com.rayshen.applib.network.mode.CacheResult;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Shu-Hua Shen on 2017/9/14.
 */
public class RSPutRequest extends RSBaseHttpRequest<RSPutRequest> {

    public RSPutRequest(String suffixUrl) {
        super(suffixUrl);
    }

    @Override
    protected <T> Observable<T> execute(Type type) {
        return apiService.put(suffixUrl, params).compose(this.<T>norTransformer(type));
    }

    @Override
    protected <T> Observable<CacheResult<T>> cacheExecute(Type type) {
        return this.<T>execute(type).compose(RxHttp.getApiCache().<T>transformer(cacheMode, type));
    }

    @Override
    protected <T> void execute(AsyncCallback<T> callback) {
        DisposableObserver disposableObserver = new ApiCallbackSubscriber(callback);
        if (super.tag != null) {
            ApiManager.get().add(super.tag, disposableObserver);
        }
        if (isLocalCache) {
            this.cacheExecute(getSubType(callback)).subscribe(disposableObserver);
        } else {
            this.execute(getType(callback)).subscribe(disposableObserver);
        }
    }
}
