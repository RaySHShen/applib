package com.rayshen.applib.network.utils;

import com.google.gson.Gson;

/**
 * Created by Shu-Hua Shen on 2017/8/3.
 */
public class GsonUtil {

    private static Gson gson;

    public static Gson gson() {
        if (gson == null) {
            synchronized (Gson.class) {
                if (gson == null) {
                    gson = new Gson();
                }
            }
        }
        return gson;
    }
}
