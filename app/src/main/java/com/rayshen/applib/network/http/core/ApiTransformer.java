package com.rayshen.applib.network.http.core;

import com.rayshen.applib.network.http.config.RSHttpGlobalConfig;
import com.rayshen.applib.network.http.func.RSApiRetryFunc;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Shu-Hua Shen on 2017/8/24.
 */
public class ApiTransformer {

    public static <T> ObservableTransformer<T, T> norTransformer() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> apiResultObservable) {
                return apiResultObservable
                        .subscribeOn(Schedulers.io())
                        .unsubscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .retryWhen(new RSApiRetryFunc(
                                RSHttpGlobalConfig.getInstance().getRetryCount(),
                                RSHttpGlobalConfig.getInstance().getRetryDelayMillis()));
            }
        };
    }

    public static <T> ObservableTransformer<T, T> norTransformer(final int retryCount, final int retryDelayMillis) {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> apiResultObservable) {
                return apiResultObservable
                        .subscribeOn(Schedulers.io())
                        .unsubscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .retryWhen(new RSApiRetryFunc(retryCount, retryDelayMillis));
            }
        };
    }

}
