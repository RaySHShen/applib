package com.rayshen.applib.network.mode;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.rayshen.applib.log.RSLog;
import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.utils.common.HexUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * Created by Shu-Hua Shen on 2017/8/12.
 */
public class CookiesStore {

    private final Map<String, ConcurrentHashMap<String, Cookie>> cookies;
    private final SharedPreferences cookiePrefs;

    public CookiesStore(Context context) {
        cookiePrefs = context.getSharedPreferences(CommonConfig.COOKIE_PREFS, 0);
        cookies = new HashMap<>();
        Map<String, ?> prefsMap = cookiePrefs.getAll();
        for (Map.Entry<String, ?> entry : prefsMap.entrySet()) {
            String[] cookieNames = TextUtils.split((String) entry.getValue(), ",");
            for (String name : cookieNames) {
                String encodedCookie = cookiePrefs.getString(name, null);
                if (encodedCookie != null) {
                    Cookie decodedCookie = decodeCookie(encodedCookie);
                    if (decodedCookie != null) {
                        if (!cookies.containsKey(entry.getKey())) {
                            cookies.put(entry.getKey(), new ConcurrentHashMap<String, Cookie>());
                        }
                        cookies.get(entry.getKey()).put(name, decodedCookie);
                    }
                }
            }
        }
    }

    private String getCookieToken(Cookie cookie) {
        return cookie.name() + "@" + cookie.domain();
    }

    public void add(HttpUrl url, Cookie cookie) {
        String name = getCookieToken(cookie);

        if (!cookies.containsKey(url.host())) {
            cookies.put(url.host(), new ConcurrentHashMap<String, Cookie>());
        }

        boolean hasExpired = cookie.persistent() && ((cookie.expiresAt() - System.currentTimeMillis()) < 0);
        if (hasExpired) {
            if (cookies.containsKey(url.host())) {
                cookies.get(url.host()).remove(name);
            }
        } else {
            cookies.get(url.host()).put(name, cookie);
        }

        SharedPreferences.Editor prefsWriter = cookiePrefs.edit();
        if (cookie.persistent()) {
            prefsWriter.putString(url.host(), TextUtils.join(",", cookies.get(url.host()).keySet()));
            prefsWriter.putString(name, encodeCookie(new OkHttpCookies(cookie)));
            prefsWriter.apply();
        } else {
            prefsWriter.remove(url.host());
            prefsWriter.remove(name);
            prefsWriter.apply();
        }
    }

    public List<Cookie> get(HttpUrl url) {
        ArrayList<Cookie> ret = new ArrayList<>();
        if (cookies.containsKey(url.host())) ret.addAll(cookies.get(url.host()).values());
        return ret;
    }

    public boolean removeAll() {
        SharedPreferences.Editor prefsWriter = cookiePrefs.edit();
        prefsWriter.clear();
        prefsWriter.apply();
        cookies.clear();
        return true;
    }

    public boolean remove(HttpUrl url, Cookie cookie) {
        String name = getCookieToken(cookie);

        if (cookies.containsKey(url.host()) && cookies.get(url.host()).containsKey(name)) {
            cookies.get(url.host()).remove(name);

            SharedPreferences.Editor prefsWriter = cookiePrefs.edit();
            if (cookiePrefs.contains(name)) {
                prefsWriter.remove(name);
            }
            prefsWriter.putString(url.host(), TextUtils.join(",", cookies.get(url.host()).keySet()));
            prefsWriter.apply();

            return true;
        } else {
            return false;
        }
    }

    public List<Cookie> getCookies() {
        ArrayList<Cookie> ret = new ArrayList<>();
        for (String key : cookies.keySet())
            ret.addAll(cookies.get(key).values());

        return ret;
    }

    private String encodeCookie(OkHttpCookies cookie) {
        if (cookie == null) return null;
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(os);
            outputStream.writeObject(cookie);
        } catch (IOException e) {
            RSLog.d(RxHttp.TAG, "IOException in encodeCookie" + e.getMessage());
            return null;
        }

        return HexUtil.encodeHexStr(os.toByteArray());
    }

    private Cookie decodeCookie(String cookieString) {
        byte[]               bytes                = HexUtil.decodeHex(cookieString.toCharArray());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        Cookie               cookie               = null;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            cookie = ((OkHttpCookies) objectInputStream.readObject()).getCookies();
        } catch (IOException e) {
            RSLog.e(RxHttp.TAG, "IOException in decodeCookie" + e.getMessage());
        } catch (ClassNotFoundException e) {
            RSLog.e(RxHttp.TAG, "ClassNotFoundException in decodeCookie" + e.getMessage());
        }

        return cookie;
    }
}
