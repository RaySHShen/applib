package com.rayshen.applib.network.http.strategy.base;

import com.rayshen.applib.log.RSLog;
import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.network.http.core.ApiCache;
import com.rayshen.applib.network.mode.CacheResult;
import com.rayshen.applib.network.utils.GsonUtil;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Shu-Hua Shen on 2017/9/4.
 */
public abstract class RSBaseCacheStrategy <T> implements ICacheStrategy<T> {

    protected <T> Observable<CacheResult<T>> loadCache(final ApiCache apiCache, final String key, final Type type) {
        return apiCache.<T>get(key).filter(new Predicate<String>() {
            @Override
            public boolean test(String s) throws Exception {
                return s != null;
            }
        }).map(new Function<String, CacheResult<T>>() {
            @Override
            public CacheResult<T> apply(String s) throws Exception {
                T t = GsonUtil.gson().fromJson(s, type);
                RSLog.i(RxHttp.TAG, "loadCache result=" + t);
                return new CacheResult<>(true, t);
            }
        });
    }

    protected <T> Observable<CacheResult<T>> loadRemote(final ApiCache apiCache, final String key, Observable<T> source) {
        return source.map(new Function<T, CacheResult<T>>() {
            @Override
            public CacheResult<T> apply(T t) throws Exception {
                RSLog.i(RxHttp.TAG, "loadRemote result=" + t);
                apiCache.put(key, t).subscribeOn(Schedulers.io()).subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean status) throws Exception {
                        RSLog.i(RxHttp.TAG, "save status => " + status);
                    }
                });
                return new CacheResult<>(false, t);
            }
        });
    }
}
