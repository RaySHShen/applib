package com.rayshen.applib.network.http.strategy.base;

import com.rayshen.applib.network.http.core.ApiCache;
import com.rayshen.applib.network.mode.CacheResult;

import java.lang.reflect.Type;

import io.reactivex.Observable;

/**
 * Created by Shu-Hua Shen on 2017/9/4.
 */
public interface ICacheStrategy<T> {
    <T> Observable<CacheResult<T>> execute(ApiCache apiCache, String cacheKey, Observable<T> source, Type type);
}
