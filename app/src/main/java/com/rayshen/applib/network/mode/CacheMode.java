package com.rayshen.applib.network.mode;

/**
 * Created by Shu-Hua Shen on 2017/8/13.
 */
public enum CacheMode {
    /**
     * First network request，if failed, then use cache
     */
    FIRST_REMOTE("FirstRemoteStrategy"),

    /**
     * First load cache，if no cache, then use network request
     */
    FIRST_CACHE("FirstCacheStrategy"),

    /**
     * Only load network，but data will keep to cache
     */
    ONLY_REMOTE("OnlyRemoteStrategy"),

    /**
     * Only use cache
     */
    ONLY_CACHE("OnlyCacheStrategy"),

    /**
     * cache and network use
     */
    CACHE_AND_REMOTE("CacheAndRemoteStrategy");

    private final String className;

    CacheMode(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }
}
