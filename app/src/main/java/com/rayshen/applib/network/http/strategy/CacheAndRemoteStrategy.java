package com.rayshen.applib.network.http.strategy;

import com.rayshen.applib.network.http.core.ApiCache;
import com.rayshen.applib.network.http.strategy.base.RSBaseCacheStrategy;
import com.rayshen.applib.network.mode.CacheResult;

import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.functions.Predicate;

/**
 * Created by Shu-Hua Shen on 2017/9/5.
 */
public class CacheAndRemoteStrategy <T> extends RSBaseCacheStrategy<T> {
    @Override
    public <T> Observable<CacheResult<T>> execute(ApiCache apiCache, String cacheKey, Observable<T> source, final Type type) {
        Observable<CacheResult<T>> cache = loadCache(apiCache, cacheKey, type);
        final Observable<CacheResult<T>> remote = loadRemote(apiCache, cacheKey, source);
        return Observable.concat(cache, remote).filter(new Predicate<CacheResult<T>>() {
            @Override
            public boolean test(CacheResult<T> tCacheResult) {
                return tCacheResult != null && tCacheResult.getCacheData() != null;
            }
        });
    }
}
