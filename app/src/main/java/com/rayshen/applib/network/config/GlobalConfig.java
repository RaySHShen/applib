package com.rayshen.applib.network.config;

import com.rayshen.applib.network.imageloader.ILoader;

/**
 * Created by Shu-Hua Shen on 2017/8/9.
 */
public class GlobalConfig {

    public static final int IL_LOADING_RES = ILoader.Options.RES_NONE;
    public static final int IL_ERROR_RES = ILoader.Options.RES_NONE;

    public static final String CACHE_SP_NAME = "sp_cache";
    public static final String CACHE_DISK_DIR = "disk_cache";
    public static final String CACHE_HTTP_DIR = "http_cache";
    public static final long CACHE_NEVER_EXPIRE = -1;

    public static final int MAX_AGE_ONLINE = 60;// Default max online cache (s)
    public static final int MAX_AGE_OFFLINE = 24 * 60 * 60;// Default max offline cache (s)

    public static final String COOKIE_PREFS = "Cookies_Prefs";

    public static final int DEFAULT_TIMEOUT = 60;// Default timeout(s)
    public static final int DEFAULT_MAX_IDLE_CONNECTIONS = 5;
    public static final long DEFAULT_KEEP_ALIVE_DURATION = 8;//Default heart beat (s)
    public static final long CACHE_MAX_SIZE = 10 * 1024 * 1024;

    public static final int DEFAULT_RETRY_COUNT = 0;
    public static final int DEFAULT_RETRY_DELAY_MILLIS = 1000;//Default retry time（ms）

    public static final String DEFAULT_DOWNLOAD_DIR = "download";
    public static final String DEFAULT_DOWNLOAD_FILE_NAME = "download_file.tmp";

}
