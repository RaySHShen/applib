package com.rayshen.applib.network.http.interceptor;

import android.content.Context;

import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.utils.network.NetworkUtil;

import java.io.IOException;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Shu-Hua Shen on 2017/8/28.
 */
public class OfflineCacheInterceptor implements Interceptor {

    private Context context;
    private String  cacheControlValue;

    public OfflineCacheInterceptor(Context context) {
        this(context, CommonConfig.MAX_AGE_OFFLINE);
    }

    public OfflineCacheInterceptor(Context context, int cacheControlValue) {
        this.context = context;
        this.cacheControlValue = String.format("max-stale=%d", cacheControlValue);
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        if (!NetworkUtil.isConnected(context)) {
            request = request.newBuilder()
                             .cacheControl(CacheControl.FORCE_CACHE)
                             .build();
            Response response = chain.proceed(request);
            return response.newBuilder()
                           .header("Cache-Control", "public, only-if-cached, " + cacheControlValue)
                           .removeHeader("Pragma")
                           .build();
        }
        return chain.proceed(request);
    }
}
