package com.rayshen.applib.network.mode;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigInteger;
import java.text.NumberFormat;

/**
 * Created by Shu-Hua Shen on 2017/8/18.
 */
public class DownloadProgress implements Parcelable {

    public static final long       ONE_KB    = 1024;
    public static final BigInteger ONE_KB_BI = BigInteger.valueOf(ONE_KB);
    public static final BigInteger ONE_MB_BI = ONE_KB_BI.multiply(ONE_KB_BI);
    public static final BigInteger ONE_GB_BI = ONE_KB_BI.multiply(ONE_MB_BI);
    public static final BigInteger ONE_TB_BI = ONE_KB_BI.multiply(ONE_GB_BI);
    public static final BigInteger ONE_PB_BI = ONE_KB_BI.multiply(ONE_TB_BI);
    public static final BigInteger ONE_EB_BI = ONE_KB_BI.multiply(ONE_PB_BI);

    private long totalSize;
    private long downloadSize;

    public DownloadProgress() {
    }

    public DownloadProgress(Parcel in) {
        totalSize = in.readLong();
        downloadSize = in.readLong();
    }

    public DownloadProgress(long totalSize, long downloadSize) {
        this.totalSize = totalSize;
        this.downloadSize = downloadSize;
    }

    public long getTotalSize() {
        return totalSize;
    }

    public DownloadProgress setTotalSize(long totalSize) {
        this.totalSize = totalSize;
        return this;
    }

    public long getDownloadSize() {
        return downloadSize;
    }

    public DownloadProgress setDownloadSize(long downloadSize) {
        this.downloadSize = downloadSize;
        return this;
    }

    public boolean isDownloadComplete() {
        return downloadSize == totalSize;
    }

    /**
     * get format total size
     *
     * @return example: 2KB , 10MB
     */
    public String getFormatTotalSize() {
        return byteCountToDisplaySize(totalSize);
    }

    /**
     * get format current size
     *
     * @return
     */
    public String getFormatDownloadSize() {
        return byteCountToDisplaySize(downloadSize);
    }

    /**
     * get format download status
     *
     * @return example: 2MB/36MB
     */
    public String getFormatStatusString() {
        return getFormatDownloadSize() + "/" + getFormatTotalSize();
    }

    /**
     * get download percentage
     *
     * @return example: 5.25%
     */
    public String getPercent() {
        String percent;
        Double result;
        if (totalSize == 0L) {
            result = 0.0;
        } else {
            result = downloadSize * 1.0 / totalSize;
        }
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMinimumFractionDigits(2);
        percent = nf.format(result);
        return percent;
    }

    public static String byteCountToDisplaySize(BigInteger size) {
        String displaySize;

        if (size.divide(ONE_EB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_EB_BI)) + " EB";
        } else if (size.divide(ONE_PB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_PB_BI)) + " PB";
        } else if (size.divide(ONE_TB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_TB_BI)) + " TB";
        } else if (size.divide(ONE_GB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_GB_BI)) + " GB";
        } else if (size.divide(ONE_MB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_MB_BI)) + " MB";
        } else if (size.divide(ONE_KB_BI).compareTo(BigInteger.ZERO) > 0) {
            displaySize = String.valueOf(size.divide(ONE_KB_BI)) + " KB";
        } else {
            displaySize = String.valueOf(size) + " bytes";
        }
        return displaySize;
    }

    public static String byteCountToDisplaySize(long size) {
        return byteCountToDisplaySize(BigInteger.valueOf(size));
    }

    public static final Creator<DownloadProgress> CREATOR = new Creator<DownloadProgress>() {
        @Override
        public DownloadProgress createFromParcel(Parcel in) {
            return new DownloadProgress(in);
        }

        @Override
        public DownloadProgress[] newArray(int size) {
            return new DownloadProgress[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(totalSize);
        dest.writeLong(downloadSize);
    }

    @Override
    public String toString() {
        return "DownloadProgress{" +
               "downloadSize=" + downloadSize +
               ", totalSize=" + totalSize +
               '}';
    }
}
