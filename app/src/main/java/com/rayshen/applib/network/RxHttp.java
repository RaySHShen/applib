package com.rayshen.applib.network;

import android.content.Context;

import com.rayshen.applib.network.http.callback.UploadCallback;
import com.rayshen.applib.network.http.config.RSHttpGlobalConfig;
import com.rayshen.applib.network.http.core.ApiCache;
import com.rayshen.applib.network.http.core.ApiManager;
import com.rayshen.applib.network.http.request.RSDeleteRequest;
import com.rayshen.applib.network.http.request.RSDownloadRequest;
import com.rayshen.applib.network.http.request.RSGetRequest;
import com.rayshen.applib.network.http.request.RSHeaderRequest;
import com.rayshen.applib.network.http.request.RSOptionsRequest;
import com.rayshen.applib.network.http.request.RSPatchRequest;
import com.rayshen.applib.network.http.request.RSPostRequest;
import com.rayshen.applib.network.http.request.RSPutRequest;
import com.rayshen.applib.network.http.request.RSRetrofitRequest;
import com.rayshen.applib.network.http.request.RSUploadRequest;
import com.rayshen.applib.network.http.request.base.RSBaseHttpRequest;

import io.reactivex.disposables.Disposable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * Created by Shu-Hua Shen on 2018/4/9.
 */
public class RxHttp {

    public final static String TAG = "RxHttp";

    private static Context              context;
    private static OkHttpClient.Builder okHttpBuilder;
    private static Retrofit.Builder     retrofitBuilder;
    private static ApiCache.Builder     apiCacheBuilder;
    private static OkHttpClient         okHttpClient;

    private static final RSHttpGlobalConfig NET_GLOBAL_CONFIG = RSHttpGlobalConfig.getInstance();

    public static RSHttpGlobalConfig getConfig() {
        return NET_GLOBAL_CONFIG;
    }

    public static void init(Context appContext) {
        if (context == null && appContext != null) {
            context = appContext.getApplicationContext();
            okHttpBuilder = new OkHttpClient.Builder();
            retrofitBuilder = new Retrofit.Builder();
            apiCacheBuilder = new ApiCache.Builder(context);
        }
    }

    public static Context getContext() {
        if (context == null) {
            throw new IllegalStateException("Please call RXHttp.init(this) in Application to initialize!");
        }
        return context;
    }

    public static OkHttpClient.Builder getOkHttpBuilder() {
        if (okHttpBuilder == null) {
            throw new IllegalStateException("Please call RXHttp.init(this) in Application to initialize!");
        }
        return okHttpBuilder;
    }

    public static Retrofit.Builder getRetrofitBuilder() {
        if (retrofitBuilder == null) {
            throw new IllegalStateException("Please call RXHttp.init(this) in Application to initialize!");
        }
        return retrofitBuilder;
    }

    public static ApiCache.Builder getApiCacheBuilder() {
        if (apiCacheBuilder == null) {
            throw new IllegalStateException("Please call RXHttp.init(this) in Application to initialize!");
        }
        return apiCacheBuilder;
    }

    public static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            okHttpClient = getOkHttpBuilder().build();
        }
        return okHttpClient;
    }

    public static ApiCache getApiCache() {
        return getApiCacheBuilder().build();
    }

    /**
     * common request，define by yourself
     *
     * @param request
     * @return
     */
    public static RSBaseHttpRequest base(RSBaseHttpRequest request) {
        if (request != null) {
            return request;
        } else {
            return new RSGetRequest("");
        }
    }

    /**
     *
     * @return
     */
    public static <T> RSRetrofitRequest retrofit() {
        return new RSRetrofitRequest();
    }

    /**
     * GET request
     *
     * @param suffixUrl
     * @return
     */
    public static RSGetRequest get(String suffixUrl) {
        return new RSGetRequest(suffixUrl);
    }

    /**
     * POST request
     *
     * @param suffixUrl
     * @return
     */
    public static RSPostRequest post(String suffixUrl) {
        return new RSPostRequest(suffixUrl);
    }

    /**
     * HEAD request
     *
     * @param suffixUrl
     * @return
     */
    public static RSHeaderRequest head(String suffixUrl) {
        return new RSHeaderRequest(suffixUrl);
    }

    /**
     * PUT request
     *
     * @param suffixUrl
     * @return
     */
    public static RSPutRequest put(String suffixUrl) {
        return new RSPutRequest(suffixUrl);
    }

    /**
     * PATCH request
     *
     * @param suffixUrl
     * @return
     */
    public static RSPatchRequest patch(String suffixUrl) {
        return new RSPatchRequest(suffixUrl);
    }

    /**
     * OPTIONS request
     *
     * @param suffixUrl
     * @return
     */
    public static RSOptionsRequest options(String suffixUrl) { return new RSOptionsRequest(suffixUrl); }

    /**
     * DELETE request
     *
     * @param suffixUrl
     * @return
     */
    public static RSDeleteRequest delete(String suffixUrl) { return new RSDeleteRequest(suffixUrl); }

    /**
     * upload
     *
     * @param suffixUrl
     * @return
     */
    public static RSUploadRequest upload(String suffixUrl) { return new RSUploadRequest(suffixUrl); }

    /**
     * upload with callback
     *
     * @param suffixUrl
     * @return
     */
    public static RSUploadRequest upload(String suffixUrl, UploadCallback uCallback) {
        return new RSUploadRequest(suffixUrl, uCallback);
    }

    /**
     * download
     *
     * @param suffixUrl
     * @return
     */
    public static RSDownloadRequest download(String suffixUrl) {
        return new RSDownloadRequest(suffixUrl);
    }

    /**
     *
     * @param tag
     * @param disposable
     */
    public static void addDisposable(Object tag, Disposable disposable) {
        ApiManager.get().add(tag, disposable);
    }

    /**
     * according to the tag, cancel request
     */
    public static void cancelTag(Object tag) {
        ApiManager.get().cancel(tag);
    }

    /**
     * cancel all request
     */
    public static void cancelAll() {
        ApiManager.get().cancelAll();
    }

    /**
     * remove cache
     *
     * @param key
     */
    public static void removeCache(String key) {
        getApiCache().remove(key);
    }

    /**
     * remove all cache and close all cache
     *
     * @return
     */
    public static Disposable clearCache() {
        return getApiCache().clear();
    }

}
