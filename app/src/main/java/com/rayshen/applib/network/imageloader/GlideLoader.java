package com.rayshen.applib.network.imageloader;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ViewTarget;
import com.rayshen.applib.log.RSLog;

import java.io.File;

/**
 * Created by Shu-Hua Shen on 2017/8/12.
 */
public class GlideLoader implements ILoader
{

    private RequestManager getRequestManager(Context context) {
        return Glide.with(context);
    }

    private void load(ViewTarget viewTarget) {
        if (viewTarget == null) {
            RSLog.d("GlideLoader", "viewTarget is null");
            return;
        }
    }

    private RequestOptions createDefaultOptions(Options options) {
        RequestOptions requestOptions = new RequestOptions();
        if (options == null) options = Options.defaultOptions();
        if (options.loadingResId != Options.RES_NONE) {
            requestOptions.placeholder(options.loadingResId);
        }
        if (options.loadErrorResId != Options.RES_NONE) {
            requestOptions.error(options.loadErrorResId);
        }
        return requestOptions;
    }

    private RequestOptions createCenterCropOptions(Options options) {
        RequestOptions requestOptions = new RequestOptions();
        if (options == null) options = Options.defaultOptions();
        if (options.loadingResId != Options.RES_NONE) {
            requestOptions.placeholder(options.loadingResId);
        }
        if (options.loadErrorResId != Options.RES_NONE) {
            requestOptions.error(options.loadErrorResId);
        }
        requestOptions.centerCrop();
        return requestOptions;
    }

    private RequestOptions createFitCenterOptions(Options options) {
        RequestOptions requestOptions = new RequestOptions();
        if (options == null) options = Options.defaultOptions();
        if (options.loadingResId != Options.RES_NONE) {
            requestOptions.placeholder(options.loadingResId);
        }
        if (options.loadErrorResId != Options.RES_NONE) {
            requestOptions.error(options.loadErrorResId);
        }
        requestOptions.fitCenter();
        return requestOptions;
    }

    private RequestOptions createCenterInsideOptions(Options options) {
        RequestOptions requestOptions = new RequestOptions();
        if (options == null) options = Options.defaultOptions();
        if (options.loadingResId != Options.RES_NONE) {
            requestOptions.placeholder(options.loadingResId);
        }
        if (options.loadErrorResId != Options.RES_NONE) {
            requestOptions.error(options.loadErrorResId);
        }
        requestOptions.centerInside();
        return requestOptions;
    }

    private RequestOptions createCircleCropOptions(Options options) {
        RequestOptions requestOptions = new RequestOptions();
        if (options == null) options = Options.defaultOptions();
        if (options.loadingResId != Options.RES_NONE) {
            requestOptions.placeholder(options.loadingResId);
        }
        if (options.loadErrorResId != Options.RES_NONE) {
            requestOptions.error(options.loadErrorResId);
        }
        requestOptions.circleCrop();
        return requestOptions;
    }

    @Override
    public void init(Context context) {
        try {
            Class.forName("com.bumptech.glide.Glide");
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException("Must be dependencies Glide!");
        }
    }

    @Override
    public void loadNet(ImageView target, String url, Options options) {
        load(getRequestManager(target.getContext()).load(url).apply(createDefaultOptions(options)).into(target));
    }

    @Override
    public void loadResource(ImageView target, int resId, Options options) {
        load(getRequestManager(target.getContext()).load(resId).apply(createDefaultOptions(options)).into(target));
    }

    @Override
    public void loadAssets(ImageView target, String assetName, Options options) {
        load(getRequestManager(target.getContext()).load("file:///android_asset/" + assetName).apply(createDefaultOptions(options)).into(target));
    }

    @Override
    public void loadFile(ImageView target, File file, Options options) {
        load(getRequestManager(target.getContext()).load(file).apply(createDefaultOptions(options)).into(target));
    }

    @Override
    public void loadNetCenterCrop(ImageView target, String url, Options options) {
        load(getRequestManager(target.getContext()).load(url).apply(createCenterCropOptions(options)).into(target));
    }

    @Override
    public void loadResourceCenterCrop(ImageView target, int resId, Options options) {
        load(getRequestManager(target.getContext()).load(resId).apply(createCenterCropOptions(options)).into(target));
    }

    @Override
    public void loadAssetsCenterCrop(ImageView target, String assetName, Options options) {
        load(getRequestManager(target.getContext()).load("file:///android_asset/" + assetName).apply(createCenterCropOptions(options)).into(target));
    }

    @Override
    public void loadFileCenterCrop(ImageView target, File file, Options options) {
        load(getRequestManager(target.getContext()).load(file).apply(createCenterCropOptions(options)).into(target));
    }

    @Override
    public void loadNetFitCenter(ImageView target, String url, Options options) {
        load(getRequestManager(target.getContext()).load(url).apply(createFitCenterOptions(options)).into(target));
    }

    @Override
    public void loadResourceFitCenter(ImageView target, int resId, Options options) {
        load(getRequestManager(target.getContext()).load(resId).apply(createFitCenterOptions(options)).into(target));
    }

    @Override
    public void loadAssetsFitCenter(ImageView target, String assetName, Options options) {
        load(getRequestManager(target.getContext()).load("file:///android_asset/" + assetName).apply(createFitCenterOptions(options)).into(target));
    }

    @Override
    public void loadFileFitCenter(ImageView target, File file, Options options) {
        load(getRequestManager(target.getContext()).load(file).apply(createFitCenterOptions(options)).into(target));
    }

    @Override
    public void loadNetCenterInside(ImageView target, String url, Options options) {
        load(getRequestManager(target.getContext()).load(url).apply(createCenterInsideOptions(options)).into(target));
    }

    @Override
    public void loadResourceCenterInside(ImageView target, int resId, Options options) {
        load(getRequestManager(target.getContext()).load(resId).apply(createCenterInsideOptions(options)).into(target));
    }

    @Override
    public void loadAssetsCenterInside(ImageView target, String assetName, Options options) {
        load(getRequestManager(target.getContext()).load("file:///android_asset/" + assetName).apply(createCenterInsideOptions(options)).into(target));
    }

    @Override
    public void loadFileCenterInside(ImageView target, File file, Options options) {
        load(getRequestManager(target.getContext()).load(file).apply(createCenterInsideOptions(options)).into(target));
    }

    @Override
    public void loadNetCircleCrop(ImageView target, String url, Options options) {
        load(getRequestManager(target.getContext()).load(url).apply(createCircleCropOptions(options)).into(target));
    }

    @Override
    public void loadResourceCircleCrop(ImageView target, int resId, Options options) {
        load(getRequestManager(target.getContext()).load(resId).apply(createCircleCropOptions(options)).into(target));
    }

    @Override
    public void loadAssetsCircleCrop(ImageView target, String assetName, Options options) {
        load(getRequestManager(target.getContext()).load("file:///android_asset/" + assetName).apply(createCircleCropOptions(options)).into(target));
    }

    @Override
    public void loadFileCircleCrop(ImageView target, File file, Options options) {
        load(getRequestManager(target.getContext()).load(file).apply(createCircleCropOptions(options)).into(target));
    }

    @Override
    public void clearMemoryCache(Context context) {
        Glide.get(context).clearMemory();
    }

    @Override
    public void clearDiskCache(Context context) {
        Glide.get(context).clearDiskCache();
    }

    @Override
    public void resumeRequests(Context context) {
        getRequestManager(context).resumeRequests();
    }

    @Override
    public void pauseRequests(Context context) {
        getRequestManager(context).pauseRequests();
    }

    @Override
    public RequestBuilder<File> downloadOnly(Context context) {
        return getRequestManager(context).downloadOnly();
    }

    @Override
    public RequestBuilder<File> downloadOnly(Context context, Object model) {
        return getRequestManager(context).download(model);
    }
}
