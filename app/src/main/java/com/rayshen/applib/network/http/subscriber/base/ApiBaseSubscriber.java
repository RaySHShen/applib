package com.rayshen.applib.network.http.subscriber.base;

import com.rayshen.applib.network.http.exception.ApiCode;
import com.rayshen.applib.network.http.exception.ApiException;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by Shu-Hua Shen on 2017/9/2.
 */
public abstract class ApiBaseSubscriber<T> extends DisposableObserver<T> {

    protected abstract void onError(ApiException e);

    @Override
    public void onError(Throwable e) {
        if (e instanceof ApiException) {
            onError((ApiException) e);
        } else {
            onError(new ApiException(e, ApiCode.Request.UNKNOWN));
        }
    }
}
