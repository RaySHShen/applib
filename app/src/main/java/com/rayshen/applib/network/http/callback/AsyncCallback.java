package com.rayshen.applib.network.http.callback;

/**
 * Created by Shu-Hua Shen on 2017/8/21.
 */
public abstract class AsyncCallback<T> {
    public abstract void onSuccess(T data);
    public abstract void onFail(int errCode, String errMsg);
}
