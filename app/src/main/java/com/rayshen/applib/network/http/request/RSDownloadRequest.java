package com.rayshen.applib.network.http.request;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.rayshen.applib.network.config.CommonConfig;
import com.rayshen.applib.network.RxHttp;
import com.rayshen.applib.network.http.callback.AsyncCallback;
import com.rayshen.applib.network.http.core.ApiManager;
import com.rayshen.applib.network.http.func.RSApiRetryFunc;
import com.rayshen.applib.network.http.request.base.RSBaseHttpRequest;
import com.rayshen.applib.network.http.subscriber.DownloadCallbackSubscriber;
import com.rayshen.applib.network.mode.CacheResult;
import com.rayshen.applib.network.mode.DownloadProgress;

import org.reactivestreams.Publisher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by Shu-Hua Shen on 2017/9/12.
 */
public class RSDownloadRequest extends RSBaseHttpRequest<RSDownloadRequest> {

    private String rootName;
    private String dirName = CommonConfig.DEFAULT_DOWNLOAD_DIR;
    private String fileName = CommonConfig.DEFAULT_DOWNLOAD_FILE_NAME;

    public RSDownloadRequest(String suffixUrl) {
        super(suffixUrl);
        rootName = getDiskCachePath(RxHttp.getContext());
    }

    /**
     * root folder settings, default app cache folder,
     * if the device has sd card, default is sd card
     *
     * @param rootName
     * @return
     */
    public RSDownloadRequest setRootName(String rootName) {
        if (!TextUtils.isEmpty(rootName)) {
            this.rootName = rootName;
        }
        return this;
    }

    /**
     * file path settings
     *
     * @param dirName
     * @return
     */
    public RSDownloadRequest setDirName(String dirName) {
        if (!TextUtils.isEmpty(dirName)) {
            this.dirName = dirName;
        }
        return this;
    }

    /**
     * file name settings
     *
     * @param fileName
     * @return
     */
    public RSDownloadRequest setFileName(String fileName) {
        if (!TextUtils.isEmpty(fileName)) {
            this.fileName = fileName;
        }
        return this;
    }

    private void saveFile(FlowableEmitter<? super DownloadProgress> sub, File saveFile, ResponseBody resp) {
        InputStream  inputStream  = null;
        OutputStream outputStream = null;
        try {
            try {
                int readLen;
                int downloadSize = 0;
                byte[] buffer = new byte[8192];

                DownloadProgress downProgress = new DownloadProgress();
                inputStream = resp.byteStream();
                outputStream = new FileOutputStream(saveFile);

                long contentLength = resp.contentLength();
                downProgress.setTotalSize(contentLength);

                while ((readLen = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, readLen);
                    downloadSize += readLen;
                    downProgress.setDownloadSize(downloadSize);
                    sub.onNext(downProgress);
                }
                outputStream.flush();
                sub.onComplete();
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
                if (resp != null) {
                    resp.close();
                }
            }
        } catch (IOException e) {
            sub.onError(e);
        }
    }

    private File getDiskCacheDir(String rootName, String dirName) {
        return new File(rootName + File.separator + dirName);
    }

    private String getDiskCachePath(Context context) {
        String cachePath;
        if ((Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                    || !Environment.isExternalStorageRemovable())
            && context.getExternalCacheDir() != null) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
    }

    @Override
    protected <T> Observable<T> execute(Type type) {
        return (Observable<T>) apiService
                .downFile(suffixUrl, params)
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .toFlowable(BackpressureStrategy.LATEST)
                .flatMap(new Function<ResponseBody, Publisher<?>>() {
                    @Override
                    public Publisher<?> apply(final ResponseBody responseBody) throws Exception {
                        return Flowable.create(new FlowableOnSubscribe<DownloadProgress>() {
                            @Override
                            public void subscribe(FlowableEmitter<DownloadProgress> subscriber) throws Exception {
                                File dir = getDiskCacheDir(rootName, dirName);
                                if (!dir.exists()) {
                                    dir.mkdirs();
                                }
                                File file = new File(dir.getPath() + File.separator + fileName);
                                saveFile(subscriber, file, responseBody);
                            }
                        }, BackpressureStrategy.LATEST);
                    }
                })
                .sample(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .toObservable()
                .retryWhen(new RSApiRetryFunc(retryCount, retryDelayMillis));
    }

    @Override
    protected <T> Observable<CacheResult<T>> cacheExecute(Type type) {
        return null;
    }

    @Override
    protected <T> void execute(AsyncCallback<T> callback) {
        DisposableObserver disposableObserver = new DownloadCallbackSubscriber(callback);
        if (super.tag != null) {
            ApiManager.get().add(super.tag, disposableObserver);
        }
        this.execute(getType(callback)).subscribe(disposableObserver);
    }
}
