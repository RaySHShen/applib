package com.rayshen.applib.databinding.base;

import android.content.Context;
import android.databinding.BaseObservable;

/**
 * {@link T} is the class of your model for all your basics VI
 * Created by Shu-Hua Shen on 2018/3/9.
 */
public abstract class RSBaseViewModel<T> extends BaseObservable {

    /***
     *
     */
    protected final Context context;

    /**
     * Class Model used in your IVM
     */
    protected T model;

    public abstract void onCreate();


    /***
     * @param context
     * @param model
     */
    public RSBaseViewModel(Context context, T model) {
        this.context = context;
        this.model = model;
        onCreate();
    }

    public void onResume() {

    }


    public void onPause() {

    }


    public void onDestroy() {

    }

    public T getModel() {
        return model;
    }
}
