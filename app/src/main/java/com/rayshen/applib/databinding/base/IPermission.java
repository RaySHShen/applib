package com.rayshen.applib.databinding.base;

import android.support.annotation.NonNull;

/**
 * {@link IPermission} will handle the permission result from the activity
 * Created by Shu-Hua Shen on 2018/3/9.
 */
public interface IPermission {
    void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults);
}
