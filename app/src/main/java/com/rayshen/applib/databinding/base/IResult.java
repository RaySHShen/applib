package com.rayshen.applib.databinding.base;

import android.content.Intent;

/**
 * {@link IResult} will handle the onActivityResult from the activity
 * Created by Shu-Hua Shen on 2018/3/9.
 */
public interface IResult {
    void onActivityResult(int requestCode, int resultCode, Intent data);
}
