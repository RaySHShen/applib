package com.rayshen.applib.databinding.base;

import android.content.Intent;

/**
 * {@link INewIntent} is used for the activity VM that needs to handle newIntent from the activity
 * Created by Shu-Hua Shen on 2018/3/9.
 */
public interface INewIntent {
    void onNewIntent(Intent intent);
}
