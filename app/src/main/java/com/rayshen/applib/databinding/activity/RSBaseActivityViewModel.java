package com.rayshen.applib.databinding.activity;

import android.databinding.BaseObservable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.rayshen.applib.databinding.base.IViewModel;

/**
 * {@link RSBaseActivity} will allow you to generate a basic activity that put your view model {@link B} and its {@link IViewModel}
 * Created by Shu-Hua Shen on 2018/3/13.
 */
public class RSBaseActivityViewModel<A extends RSBaseActivity, B extends ViewDataBinding> extends BaseObservable
        implements IViewModel
{

    protected final Handler handler;

    protected final Handler uiHandler;

    protected A activity;

    protected B binding;

    /***
     * @param activity
     * @param binding
     */
    public RSBaseActivityViewModel(A activity, B binding, @Nullable Bundle savedInstance) {
        this.activity = activity;
        this.binding = binding;
        this.handler = new Handler();
        this.uiHandler = new Handler(Looper.getMainLooper());
        onCreate(savedInstance);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstance) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }

    /***
     * Manage the backpressed
     *
     * @return true if u want to call super
     */
    protected boolean onBackPressed() {
        return true;
    }

    /**
     * PostCreate to use if you need the savedInstanceState
     *
     * @param savedInstanceState
     */
    public void onPostCreate(Bundle savedInstanceState) {
    }

    /**
     * Manage item
     *
     * @param item
     * @return
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    /**
     * Handle animation of sharedelement
     */
    public void onEnterAnimationComplete() {

    }

}
