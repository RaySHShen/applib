package com.rayshen.applib.databinding.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Shu-Hua Shen on 2018/3/9.
 */
public interface IViewModel {
    /***
     * This method is used to instantiate the data of your viewmodel / its components
     */
    void onCreate(@Nullable Bundle savedInstance);

    void onResume();

    void onPause();

    void onDestroy();
}
