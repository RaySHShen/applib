package com.rayshen.applib.databinding.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rayshen.applib.databinding.base.IPermission;
import com.rayshen.applib.databinding.base.IResult;

/**
 * Created by Shu-Hua Shen on 2018/3/15.
 */
public abstract class RSBaseFragment<B extends ViewDataBinding, VM extends RSBaseFragmentBaseViewModel> extends Fragment
{
    /***
     * FragmentBinding used in this view
     */
    protected B binding;

    /***
     * The view model that will be used to handle this fragment
     */
    protected VM vm;

    /**
     * @return
     */
    public abstract int data();

    /***
     * @return your layout resources
     */
    @LayoutRes
    public abstract int getLayoutId();

    /***
     * @param binding
     * @return the {@link VM} you want to use in this activity
     */
    public abstract VM baseFragmentVM(B binding, @Nullable Bundle savedInstance);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        vm = baseFragmentVM(binding, savedInstanceState);
        binding.setVariable(data(), vm);
        return binding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (vm != null) {
            vm.onResume();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (vm != null) {
            vm.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (vm != null) {
            vm.onDestroy();
        }
    }

    /***
     * Handle the permission and give it to the activity
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (vm != null) {
            if (vm instanceof IPermission) {
                ((IPermission) vm).onRequestPermissionsResult(
                        requestCode,
                        permissions,
                        grantResults
                );
            }
        }
    }

    /***
     * Handle the activity result if you need to use it inside the vm of the fragment
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (vm != null) {
            if (vm instanceof IResult) {
                ((IResult) vm).onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}

