package com.rayshen.applib.databinding.fragment.v4;

import android.databinding.BaseObservable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;

import com.rayshen.applib.databinding.base.IViewModel;

/***
 * @param <F> Class that need to extends {@link RSBaseDialogFragment}
 * @param <B> Class that need to extends {@link ViewDataBinding}
 * Created by Shu-Hua Shen on 2018/3/15.
 */
public class RSBaseDialogFragmentViewModel<F extends RSBaseDialogFragment, B extends ViewDataBinding> extends BaseObservable implements IViewModel
{
    protected final Handler uiHandler;
    protected final Handler handler;

    /***
     * {@link F} is the fragment that use the current VM
     */
    protected F fragment;
    /**
     * {@link B} will be used to find the views inside the fragment
     */
    protected B binding;

    /***
     * @param
     * @param binding
     */
    public RSBaseDialogFragmentViewModel(F fragment, B binding, @Nullable Bundle savedInstance) {
        this.fragment = fragment;
        this.binding = binding;
        this.handler = new Handler();
        this.uiHandler = new Handler(Looper.getMainLooper());
        onCreate(savedInstance);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstance) {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }
}
