package com.rayshen.applib.mvp.model;

import android.support.annotation.UiThread;

/**
 * Created by Shu-Hua Shen on 2017/10/3.
 */
public interface IBasePresenter<V extends IBaseView>
{

    @UiThread
    void attachView(V view);

    @UiThread
    void detachView();
}
