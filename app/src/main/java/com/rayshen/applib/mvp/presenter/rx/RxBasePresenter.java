package com.rayshen.applib.mvp.presenter.rx;

import com.rayshen.applib.mvp.model.IBaseView;
import com.rayshen.applib.mvp.presenter.BasePresenter;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by Shu-Hua Shen on 2017/10/3.
 */
public class RxBasePresenter<V extends IBaseView> extends BasePresenter<V>
{

    protected CompositeDisposable mCompositeDisposable;

    @Override
    public void attachView(V view) {
        super.attachView(view);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
            mCompositeDisposable = null;
        }
    }

    protected void addToOnAttachDisposables(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    /**
     * Adds Disposable to <b>CompositeDisposable</b> that lives between <b>Constructor</b> and <b>onDestroy()</b>.
     */
    protected void addToOnCreateDisposables(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

}
