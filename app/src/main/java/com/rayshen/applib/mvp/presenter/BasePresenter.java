package com.rayshen.applib.mvp.presenter;

import android.support.annotation.UiThread;

import com.rayshen.applib.mvp.model.IBasePresenter;
import com.rayshen.applib.mvp.model.IBaseView;

import java.lang.ref.WeakReference;

/**
 * Created by Shu-Hua Shen on 2017/10/3.
 */
public class BasePresenter<V extends IBaseView> implements IBasePresenter<V>
{
    private WeakReference<V> viewRef;

    @UiThread
    @Override
    public void attachView(V view) {
        viewRef = new WeakReference<>(view);
    }

    @UiThread
    public V getView() {
        return viewRef == null ? null : viewRef.get();
    }

    @UiThread
    public boolean isViewAttached() {
        return viewRef != null && viewRef.get() != null;
    }

    @UiThread
    @Override
    public void detachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }
}
