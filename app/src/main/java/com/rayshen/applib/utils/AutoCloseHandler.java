package com.rayshen.applib.utils;

import com.rayshen.applib.log.RSLog;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shu-Hua Shen on 2017/6/6.
 */
public class AutoCloseHandler {

    private final static String TAG = "AutoCloseHandler";
    private AutoCloseThread mThread;
    private long mDelayTime;

    public AutoCloseHandler(long delayTime) {
        mDelayTime = delayTime;
    }

    public void post(Runnable runnable) {
        getThread().post(runnable);
    }

    public void postDelayed(Runnable runnable, long delay) {
        getThread().postDelayed(runnable, delay);
    }

    public void stopAll() {
        getThread().stopRunning();
    }

    public boolean isRunning() {
        return (mThread != null && mThread.isAvailable());
    }

    private AutoCloseThread getThread() {
        if (mThread == null || !mThread.isAvailable()) {
            mThread = new AutoCloseThread(mDelayTime);
            mThread.start();
        }
        return mThread;
    }

    public class DelayObject implements Delayed {
        public Runnable runnable;
        private long startTime;

        public DelayObject(Runnable runnable) {
            this.runnable = runnable;
            this.startTime = System.currentTimeMillis();
        }

        public DelayObject(Runnable runnable, long delay) {
            this.runnable = runnable;
            this.startTime = System.currentTimeMillis() + delay;
        }

        @Override
        public long getDelay(TimeUnit unit) {
            long diff = startTime - System.currentTimeMillis();
            return unit.convert(diff, TimeUnit.MILLISECONDS);
        }

        public void updateDelay(long delay) {
            this.startTime = System.currentTimeMillis() + delay;
        }

        @Override
        public int compareTo(Delayed o) {
            if (this.startTime < ((DelayObject) o).startTime) {
                return -1;
            }
            if (this.startTime > ((DelayObject) o).startTime) {
                return 1;
            }
            return 0;
        }
    }

    class AutoCloseThread extends Thread {
        DelayQueue<DelayObject> mQueue = new DelayQueue<>();
        private boolean mIsAvailable = true;
        long mDelayTime;
        DelayObject mFinishCommand = new DelayObject(new Runnable() {
            @Override
            public void run() {
                mIsAvailable = false;
            }
        });

        AutoCloseThread(long delayTime) {
            mDelayTime = delayTime;
        }

        public void post(Runnable runnable) {
            updateFinishTime();
            DelayObject obj = new DelayObject(runnable);
            mQueue.add(obj);
        }

        public void postDelayed(Runnable runnable, long delay) {
            updateFinishTime();
            DelayObject obj = new DelayObject(runnable, delay);
            mQueue.add(obj);
        }

        private void updateFinishTime() {
            mQueue.remove(mFinishCommand);
            mFinishCommand.updateDelay(mDelayTime);
            mQueue.add(mFinishCommand);
        }

        private void stopRunning() {
            DelayObject obj = mQueue.poll();
            while (obj != null) {
                obj = mQueue.poll();
            }
            mQueue.add(mFinishCommand);
            mIsAvailable = false;
        }


        @Override
        public void run() {
            RSLog.i("Handler create");
            while (mIsAvailable) {
                try {
                    // Take elements out from the DelayQueue object.
                    DelayObject object = mQueue.take();
                    if (mFinishCommand == object) {
                        RSLog.v("get FINISH item from queue");
                    } else {
                        RSLog.v("get item from queue: " + object.runnable);
                    }
                    object.runnable.run();
                    updateFinishTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            RSLog.i("Handler finish");
        }

        public boolean isAvailable() {
            return mIsAvailable;
        }
    }
}
