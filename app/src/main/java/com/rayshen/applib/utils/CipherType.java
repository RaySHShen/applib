package com.rayshen.applib.utils;

/**
 * Created by Shu-Hua Shen on 2017/4/7.
 */
public enum CipherType {

    SHA("SHA"),
    MD5("MD5"),
    HMAC_MD5("HmacMD5"),
    HMAC_SHA1("HmacSHA1"),
    HMAC_SHA256("HmacSHA256"),
    HMAC_SHA384("HmacSHA384"),
    HMAC_SHA512("HmacSHA512"),
    DES("DES"),
    RSA("RSA");

    private String type;

    public String getType() {
        return type;
    }

    CipherType(String type) {
        this.type = type;
    }
}
