package com.rayshen.applib.loaders;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.rayshen.applib.recyclers.RSBaseCursorRecyclerView;


/**
 * Created by Shu-Hua Shen on 2018/3/24.
 */

public abstract class RSBaseLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {

    protected CursorLoader cursorLoader = null;

    protected RSBaseCursorRecyclerView cursorRecyclerView = null;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return  cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        cursorRecyclerView.setDataSet(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        cursorRecyclerView.setDataSet(null);
    }

    public RSBaseLoaderCallbacks setCursorLoader(CursorLoader loader) {
        cursorLoader = loader;
        return this;
    }

    public RSBaseLoaderCallbacks setRecyclerView(RSBaseCursorRecyclerView recyclerView) {
        cursorRecyclerView = recyclerView;
        return this;
    }
}
