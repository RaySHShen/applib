package com.rayshen.applib.fragments;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rayshen.applib.event.BusManager;

/**
 * Created by Shu-Hua Shen on 2018/1/24.
 */
public abstract class RSBaseFragment extends Fragment implements View.OnClickListener {

    protected Context        mContext;
    protected Resources      mResources;
    protected LayoutInflater mInflater;
    protected View           mConvertView;

    protected abstract int getLayoutId();
    protected abstract void initView(View contentView);
    protected abstract void destroyView();
    protected abstract void initData();
    protected abstract void bindEvent();
    protected abstract void processClick(View view);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        this.mResources = mContext.getResources();
        this.mInflater = LayoutInflater.from(mContext);
        if (isRegisterEvent()) {
            BusManager.getBus().register(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mConvertView = inflater.inflate(getLayoutId(), container, false);
        return mConvertView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initView(view);
        bindEvent();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        destroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (isRegisterEvent()) {
            BusManager.getBus().unregister(this);
        }
    }

    @Override
    public void onClick(View v) {
        processClick(v);
    }

    protected <E extends View> E bind(@IdRes int viewId) {
        if (mConvertView == null) {
            return null;
        }
        return (E) mConvertView.findViewById(viewId);
    }

    protected <E extends View> E bind(@NonNull View view, @IdRes int viewId) {
        return (E) view.findViewById(viewId);
    }

    protected <E extends View> void setOnClickListener(@NonNull E view) {
        view.setOnClickListener(this);
    }

    protected boolean isRegisterEvent() {
        return false;
    }

}
