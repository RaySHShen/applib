package com.rayshen.applib.log.file;

import com.rayshen.applib.log.common.LogLevel;

/**
 * Created by Shu-Hua Shen on 2017/3/24.
 */
public interface LogFileFilter {
    boolean accept(@LogLevel.LogLevelType int level, String tag, String logContent);
}
