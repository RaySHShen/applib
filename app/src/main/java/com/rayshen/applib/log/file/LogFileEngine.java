package com.rayshen.applib.log.file;

import java.io.File;

/**
 * Created by Shu-Hua Shen on 2017/3/24.
 */
public interface LogFileEngine {
    void writeToFile(File logFile, String logContent, LogFileParam params);
}
