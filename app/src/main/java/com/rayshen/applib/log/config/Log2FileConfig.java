package com.rayshen.applib.log.config;

import com.rayshen.applib.log.common.LogLevel;
import com.rayshen.applib.log.file.LogFileEngine;
import com.rayshen.applib.log.file.LogFileFilter;

import java.io.File;

/**
 * Created by Shu-Hua Shen on 2017/3/28.
 */
public interface Log2FileConfig
{
    Log2FileConfig configLog2FileEnable(boolean enable);

    Log2FileConfig configLog2FilePath(String logPath);

    Log2FileConfig configLog2FileNameFormat(String formatName);

    Log2FileConfig configLog2FileLevel(@LogLevel.LogLevelType int level);

    Log2FileConfig configLogFileEngine(LogFileEngine engine);

    Log2FileConfig configLogFileFilter(LogFileFilter fileFilter);

    File getLogFile();
}
