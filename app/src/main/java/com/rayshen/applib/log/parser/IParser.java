package com.rayshen.applib.log.parser;

import com.rayshen.applib.log.common.Constant;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public interface IParser<T>
{
    String LINE_SEPARATOR = Constant.BR;
    Class<T> parseClassType();
    String parseString(T t);
}
