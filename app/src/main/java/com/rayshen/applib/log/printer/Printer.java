package com.rayshen.applib.log.printer;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.rayshen.applib.log.RSLog;
import com.rayshen.applib.log.common.Constant;
import com.rayshen.applib.log.common.LogLevel;
import com.rayshen.applib.log.config.Log2FileConfigImpl;
import com.rayshen.applib.log.config.LogConfigImpl;
import com.rayshen.applib.log.file.LogFileParam;
import com.rayshen.applib.log.utils.ObjectUtil;
import com.rayshen.applib.log.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.IllegalFormatConversionException;
import java.util.MissingFormatArgumentException;
import java.util.UnknownFormatConversionException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * Created by Shu-Hua Shen on 2017/3/29.
 */
public class Printer implements IPrinter
{

    private LogConfigImpl      mLogConfig;
    private Log2FileConfigImpl mLog2FileConfig;
    private final ThreadLocal<String> localTags = new ThreadLocal<>();

    public Printer() {
        mLogConfig = LogConfigImpl.getInstance();
        mLog2FileConfig = Log2FileConfigImpl.getInstance();
        mLogConfig.addParserClass(Constant.DEFAULT_PARSE_CLASS);
    }

    public IPrinter setTag(String tag) {
        if (!TextUtils.isEmpty(tag) && mLogConfig.isEnable()) {
            localTags.set(tag);
        }
        return this;
    }

    private synchronized void logString(@LogLevel.LogLevelType int type, String msg, Object... args) {
        logString(type, msg, null, false, args);
    }

    private void logString(@LogLevel.LogLevelType int type, String msg, String tag, boolean isPart, Object... args) {
        if (!isPart || TextUtils.isEmpty(tag)) {
            tag = generateTag();
        }
        if (!isPart) {
            if (args != null && args.length > 0) {
                try {
                    msg = String.format(msg, args);
                } catch (MissingFormatArgumentException | IllegalFormatConversionException
                        | UnknownFormatConversionException e) {
                    printLog(LogLevel.TYPE_ERROR, tag, Log.getStackTraceString(e));
                }
            }
            writeToFile(tag, msg, type);
        }
        if (!mLogConfig.isEnable() || type < mLogConfig.getLogLevel()) {
            return;
        }
        if (msg.length() > Constant.LINE_MAX) {
            if (mLogConfig.isShowBorder()) {
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_TOP));
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_NORMAL) + getTopStackInfo());
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_CENTER));
            }
            for (String subMsg : Utils.largeStringToList(msg)) {
                logString(type, subMsg, tag, true, args);
            }
            if (mLogConfig.isShowBorder()) {
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_BOTTOM));
            }
            return;
        }

        if (mLogConfig.isShowBorder()) {
            if (isPart) {
                for (String sub : msg.split(Constant.BR)) {
                    printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_NORMAL) + sub);
                }
            } else {
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_TOP));
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_NORMAL) + getTopStackInfo());
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_CENTER));
                for (String sub : msg.split(Constant.BR)) {
                    printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_NORMAL) + sub);
                }
                printLog(type, tag, Utils.printDividingLine(Utils.DIVIDER_BOTTOM));
            }
        } else {
            printLog(type, tag, msg);
        }
    }

    private void logObject(@LogLevel.LogLevelType int type, Object object) {
        logString(type, ObjectUtil.objectToString(object));
    }

    private String generateTag() {
        String tempTag = localTags.get();
        if (!TextUtils.isEmpty(tempTag)) {
            localTags.remove();
            return tempTag;
        }
        return mLogConfig.getTagPrefix();
    }

    /**
     * get current activity stack info.
     *
     * @return
     */
    private StackTraceElement getCurrentStackTrace() {
        StackTraceElement[] trace = Thread.currentThread().getStackTrace();
        int stackOffset = getStackOffset(trace, RSLog.class);
        if (stackOffset == -1) {
            stackOffset = getStackOffset(trace, Printer.class);
            if (stackOffset == -1) {
                return null;
            }
        }
        if (mLogConfig.getMethodOffset() > 0) {
            stackOffset += mLogConfig.getMethodOffset();
        }
        StackTraceElement caller = trace[stackOffset];
        return caller;
    }

    /**
     * get top stack message
     *
     * @return
     */
    private String getTopStackInfo() {
        String customTag = mLogConfig.getFormatTag(getCurrentStackTrace());
        if (customTag != null) {
            return customTag;
        }
        StackTraceElement caller = getCurrentStackTrace();
        String stackTrace = caller.toString();
        stackTrace = stackTrace.substring(stackTrace.lastIndexOf('('), stackTrace.length());
        String tag = "%s.%s%s";
        String callerClazzName = caller.getClassName();
        callerClazzName = callerClazzName.substring(callerClazzName.lastIndexOf(".") + 1);
        tag = String.format(tag, callerClazzName, caller.getMethodName(), stackTrace);
        return tag;
    }

    private int getStackOffset(StackTraceElement[] trace, Class cla) {
        for (int i = Constant.MIN_STACK_OFFSET; i < trace.length; i++) {
            StackTraceElement e = trace[i];
            String name = e.getClassName();
            if (cla.equals(Printer.class) && i < trace.length - 1 && trace[i + 1].getClassName()
                                                                                 .equals(Printer.class.getName())) {
                continue;
            }
            if (name.equals(cla.getName())) {
                return ++i;
            }
        }
        return -1;
    }

    private void printLog(@LogLevel.LogLevelType int type, String tag, String msg) {
        if (!mLogConfig.isShowBorder()) {
            msg = getTopStackInfo() + ": " + msg;
        }
        switch (type) {
            case LogLevel.TYPE_VERBOSE:
                Log.v(tag, msg);
                break;
            case LogLevel.TYPE_DEBUG:
                Log.d(tag, msg);
                break;
            case LogLevel.TYPE_INFO:
                Log.i(tag, msg);
                break;
            case LogLevel.TYPE_WARM:
                Log.w(tag, msg);
                break;
            case LogLevel.TYPE_ERROR:
                Log.e(tag, msg);
                break;
            case LogLevel.TYPE_WTF:
                Log.wtf(tag, msg);
                break;
            default:
                break;
        }
    }

    /**
     * write log to file
     * @param tagName
     * @param logContent
     * @param logLevel
     */
    private void writeToFile(String tagName, String logContent, @LogLevel.LogLevelType int logLevel) {
        if (!mLog2FileConfig.isEnable()) {
            return;
        }
        if (mLog2FileConfig.getFileFilter() != null
            && !mLog2FileConfig.getFileFilter().accept(logLevel, tagName, logContent)) {
            return;
        }
        if (logLevel < mLog2FileConfig.getLogLevel()) {
            return;
        }
        String path = mLog2FileConfig.getLogPath();
        if (TextUtils.isEmpty(path)) {
            if (Build.VERSION.SDK_INT >= 23) {
                Log.e(tagName, "LogUtils write to logFile error. No sdcard access permission?");
                return;
            }
            throw new IllegalArgumentException("Log2FilePath is an invalid path");
        }
        File logFile = new File(path, mLog2FileConfig.getLogFormatName());
        LogFileParam param = new LogFileParam(System.currentTimeMillis(), logLevel,
                Thread.currentThread().getName(), tagName);
        if (mLog2FileConfig.getEngine() != null) {
            mLog2FileConfig.getEngine().writeToFile(logFile, logContent, param);
        } else {
            throw new NullPointerException("LogFileEngine must not Null");
        }
    }

    @Override
    public void d(String message, Object... args) {
        logString(LogLevel.TYPE_DEBUG, message, args);
    }

    @Override
    public void d(Object object) {
        logObject(LogLevel.TYPE_DEBUG, object);
    }

    @Override
    public void e(String message, Object... args) {
        logString(LogLevel.TYPE_ERROR, message, args);
    }

    @Override
    public void e(Object object) {
        logObject(LogLevel.TYPE_ERROR, object);
    }

    @Override
    public void w(String message, Object... args) {
        logString(LogLevel.TYPE_WARM, message, args);
    }

    @Override
    public void w(Object object) {
        logObject(LogLevel.TYPE_WARM, object);
    }

    @Override
    public void i(String message, Object... args) {
        logString(LogLevel.TYPE_INFO, message, args);
    }

    @Override
    public void i(Object object) {
        logObject(LogLevel.TYPE_INFO, object);
    }

    @Override
    public void v(String message, Object... args) {
        logString(LogLevel.TYPE_VERBOSE, message, args);
    }

    @Override
    public void v(Object object) {
        logObject(LogLevel.TYPE_VERBOSE, object);
    }

    @Override
    public void wtf(String message, Object... args) {
        logString(LogLevel.TYPE_WTF, message, args);
    }

    @Override
    public void wtf(Object object) {
        logObject(LogLevel.TYPE_WTF, object);
    }

    @Override
    public void json(String json) {
        int indent = 4;
        if (TextUtils.isEmpty(json)) {
            d("JSON{json is empty}");
            return;
        }
        try {
            if (json.startsWith("{")) {
                JSONObject jsonObject = new JSONObject(json);
                String     msg        = jsonObject.toString(indent);
                d(msg);
            } else if (json.startsWith("[")) {
                JSONArray jsonArray = new JSONArray(json);
                String    msg       = jsonArray.toString(indent);
                d(msg);
            }
        } catch (JSONException e) {
            e(e.toString() + "\n\njson = " + json);
        }
    }

    @Override
    public void xml(String xml) {
        if (TextUtils.isEmpty(xml)) {
            d("XML{xml is empty}");
            return;
        }
        try {
            Source       xmlInput    = new StreamSource(new StringReader(xml));
            StreamResult xmlOutput   = new StreamResult(new StringWriter());
            Transformer  transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            transformer.transform(xmlInput, xmlOutput);
            d(xmlOutput.getWriter().toString().replaceFirst(">", ">\n"));
        } catch (TransformerException e) {
            e(e.toString() + "\n\nxml = " + xml);
        }
    }
}
