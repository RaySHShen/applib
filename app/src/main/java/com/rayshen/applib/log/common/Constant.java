package com.rayshen.applib.log.common;

import com.rayshen.applib.log.config.LogConfigImpl;
import com.rayshen.applib.log.parser.BundleParser;
import com.rayshen.applib.log.parser.CollectionParser;
import com.rayshen.applib.log.parser.HeaderParser;
import com.rayshen.applib.log.parser.IParser;
import com.rayshen.applib.log.parser.IntentParser;
import com.rayshen.applib.log.parser.MapParser;
import com.rayshen.applib.log.parser.MessageParser;
import com.rayshen.applib.log.parser.OkHttpResponseParser;
import com.rayshen.applib.log.parser.ReferenceParser;
import com.rayshen.applib.log.parser.ThrowableParser;

import java.util.List;

/**
 * Created by Shu-Hua Shen on 2017/3/24.
 */
public class Constant
{
    public static final String TAG = "RSLog";

    public static final String STRING_OBJECT_NULL = "Object[object is null]";

    public static final int LINE_MAX = 2800;

    public static final int MAX_CHILD_LEVEL = 2;

    public static final int MIN_STACK_OFFSET = 5;

    public static final String BR = System.getProperty("line.separator");

    public static final String SPACE = "\t";

    public static final Class<? extends IParser>[] DEFAULT_PARSE_CLASS = new Class[] {
            BundleParser.class, IntentParser.class, CollectionParser.class,
            MapParser.class, ThrowableParser.class, ReferenceParser.class,
            MessageParser.class, OkHttpResponseParser.class, HeaderParser.class
    };

    public static final List<IParser> getParsers() {
        return LogConfigImpl.getInstance().getParseList();
    }
}
