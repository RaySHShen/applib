package com.rayshen.applib.log.parser;

import android.os.Message;

import com.rayshen.applib.log.utils.ObjectUtil;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class MessageParser implements IParser<Message>
{
    @Override
    public Class<Message> parseClassType() {
        return Message.class;
    }

    @Override
    public String parseString(Message message) {
        if (message == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder(message.getClass().getName() + " [" + LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "what", message.what)).append(LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "when", message.getWhen())).append(LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "arg1", message.arg1)).append(LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "arg2", message.arg2)).append(LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "data",
                new BundleParser().parseString(message.getData()))).append(LINE_SEPARATOR);
        stringBuilder.append(String.format("%s = %s", "obj",
                ObjectUtil.objectToString(message.obj))).append(LINE_SEPARATOR);
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
