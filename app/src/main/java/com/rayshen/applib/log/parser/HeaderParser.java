package com.rayshen.applib.log.parser;

import okhttp3.Headers;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class HeaderParser implements IParser<Headers>
{
    @Override
    public Class<Headers> parseClassType() {
        return Headers.class;
    }

    @Override
    public String parseString(Headers headers) {
        StringBuilder builder = new StringBuilder(headers.getClass().getSimpleName() + " [" + LINE_SEPARATOR);
        for (String name : headers.names()) {
            builder.append(String.format("%s = %s" + LINE_SEPARATOR,
                    name, headers.get(name)));
        }
        return builder.append("]").toString();
    }
}
