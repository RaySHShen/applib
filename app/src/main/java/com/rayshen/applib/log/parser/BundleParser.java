package com.rayshen.applib.log.parser;

import android.os.Bundle;

import com.rayshen.applib.log.utils.ObjectUtil;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class BundleParser implements IParser<Bundle>
{
    @Override
    public Class<Bundle> parseClassType() {
        return Bundle.class;
    }

    @Override
    public String parseString(Bundle bundle) {
        if (bundle != null) {
            StringBuilder builder = new StringBuilder(bundle.getClass().getName() + " [" + LINE_SEPARATOR);
            for (String key : bundle.keySet()) {
                builder.append(String.format("'%s' => %s " + LINE_SEPARATOR,
                        key, ObjectUtil.objectToString(bundle.get(key))));
            }
            builder.append("]");
            return builder.toString();
        }
        return null;
    }
}
