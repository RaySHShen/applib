package com.rayshen.applib.log.parser;

import com.rayshen.applib.log.utils.ObjectUtil;

import java.lang.ref.Reference;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class ReferenceParser implements IParser<Reference>
{
    @Override
    public Class<Reference> parseClassType() {
        return Reference.class;
    }

    @Override
    public String parseString(Reference reference) {
        Object actual = reference.get();
        StringBuilder builder = new StringBuilder(reference.getClass().getSimpleName() + "<"
                                                  + actual.getClass().getSimpleName() + "> {");
        builder.append("→" + ObjectUtil.objectToString(actual));
        return builder.toString() + "}";
    }
}
