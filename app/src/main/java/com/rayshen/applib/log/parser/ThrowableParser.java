package com.rayshen.applib.log.parser;

import android.util.Log;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class ThrowableParser implements IParser<Throwable>
{
    @Override
    public Class<Throwable> parseClassType() {
        return Throwable.class;
    }

    @Override
    public String parseString(Throwable throwable) {
        return Log.getStackTraceString(throwable);
    }
}
