package com.rayshen.applib.log.parser;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Shu-Hua Shen on 2017/3/26.
 */
public class OkHttpResponseParser implements IParser<Response>
{
    @Override
    public Class<Response> parseClassType() {
        return Response.class;
    }

    @Override
    public String parseString(Response response) {
        if (response != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("code = %s" + LINE_SEPARATOR, response.code()));
            builder.append(String.format("isSuccessful = %s" + LINE_SEPARATOR, response.isSuccessful()));
            builder.append(String.format("url = %s" + LINE_SEPARATOR, response.request().url()));
            builder.append(String.format("message = %s" + LINE_SEPARATOR, response.message()));
            builder.append(String.format("protocol = %s" + LINE_SEPARATOR, response.protocol()));
            builder.append(String.format("header = %s" + LINE_SEPARATOR,
                    new HeaderParser().parseString(response.headers())));
            try {
                builder.append(String.format("body = %s" + LINE_SEPARATOR, response.body().string()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.toString();
        }
        return null;
    }
}
