package com.rayshen.applib.log.config;

import com.rayshen.applib.log.common.LogLevel;
import com.rayshen.applib.log.parser.IParser;

/**
 * Created by Shu-Hua Shen on 2017/3/28.
 */
public interface LogConfig
{
    LogConfig configAllowLog(boolean allowLog);

    LogConfig configTagPrefix(String prefix);

    LogConfig configFormatTag(String formatTag);

    LogConfig configShowBorders(boolean showBorder);

    LogConfig configMethodOffset(int offset);

    LogConfig configLevel(@LogLevel.LogLevelType int logLevel);

    LogConfig addParserClass(Class<? extends IParser> ... classes);
}
