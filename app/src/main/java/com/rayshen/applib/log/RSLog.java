package com.rayshen.applib.log;

import com.rayshen.applib.log.config.Log2FileConfig;
import com.rayshen.applib.log.config.Log2FileConfigImpl;
import com.rayshen.applib.log.config.LogConfig;
import com.rayshen.applib.log.config.LogConfigImpl;
import com.rayshen.applib.log.printer.IPrinter;
import com.rayshen.applib.log.printer.Printer;

/**
 * Created by Shu-Hua Shen on 2017/3/30.
 */
public class RSLog
{
    public static final boolean DEBUG = true;

    private static Printer            printer        = new Printer();
    private static LogConfigImpl      logConfig      = LogConfigImpl.getInstance();
    private static Log2FileConfigImpl log2FileConfig = Log2FileConfigImpl.getInstance();

    public static LogConfig getLogConfig() {
        return logConfig;
    }

    public static Log2FileConfig getLog2FileConfig() {
        return log2FileConfig;
    }

    public static IPrinter tag(String tag) {
        return printer.setTag(tag);
    }

    /**
     * print verbose
     *
     * @param msg
     * @param args
     */
    public static void v(String msg, Object... args) {
        printer.v(msg, args);
    }

    public static void v(Object object) {
        printer.v(object);
    }

    /**
     * print debug
     *
     * @param msg
     * @param args
     */
    public static void d(String msg, Object... args) {
        printer.d(msg, args);
    }

    public static void d(Object object) {
        printer.d(object);
    }

    /**
     * print info
     *
     * @param msg
     * @param args
     */
    public static void i(String msg, Object... args) {
        printer.i(msg, args);
    }

    public static void i(Object object) {
        printer.i(object);
    }

    /**
     * print warn
     *
     * @param msg
     * @param args
     */
    public static void w(String msg, Object... args) {
        printer.w(msg, args);
    }

    public static void w(Object object) {
        printer.w(object);
    }

    /**
     * print error
     *
     * @param msg
     * @param args
     */
    public static void e(String msg, Object... args) {
        printer.e(msg, args);
    }

    public static void e(Object object) {
        printer.e(object);
    }

    /**
     * print assert
     *
     * @param msg
     * @param args
     */
    public static void wtf(String msg, Object... args) {
        printer.wtf(msg, args);
    }

    public static void wtf(Object object) {
        printer.wtf(object);
    }

    /**
     * print json
     *
     * @param json
     */
    public static void json(String json) {
        printer.json(json);
    }

    /**
     * print xml
     * @param xml
     */
    public static void xml(String xml) {
        printer.xml(xml);
    }
}
