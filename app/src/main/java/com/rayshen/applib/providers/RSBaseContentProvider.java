package com.rayshen.applib.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import com.rayshen.applib.log.RSLog;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shu-Hua Shen on 2018/3/26.
 */
public abstract class RSBaseContentProvider extends ContentProvider {

    // MIME Type
    protected static final String DIR_CORE_MINE_TYPE = "vnd.android.cursor.dir";
    protected static final String ITEM_CORE_MINE_TYPE = "vnd.android.cursor.item";

    protected static final String MIME_TYPE_FORMAT = "%s/%s"; // (core mine type)/(sub mine type)
    protected static final String MIME_SUBTYPE_FORMAT = "vnd.%s.%s"; // vnd.(vendor name).(table name)

    protected static final String CONTENT_PROVIDER_AUTHORITY_FORMAT = "%s.%s"; // (package name).(content provider name)
    protected static final String CONTENT_PROVIDER_URI_FORMAT  = "content://%s/%s";

    protected SparseArray<String> mimeTypes = new SparseArray<>();
    protected UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);;

    protected RSBaseSQLiteOpenHelper sqliteHelper;

    @NonNull
    protected abstract RSBaseSQLiteOpenHelper createSQLiteHelper();

    @NonNull
    protected abstract void setupMimeTypes();

    @NonNull
    protected abstract void setupURIMatcher();

    /**
     * Constructor
     */
    public RSBaseContentProvider() {
        super();
        setupMimeTypes();
        setupURIMatcher();
    }

    @Override
    public boolean onCreate() {
        sqliteHelper = createSQLiteHelper();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(
            @NonNull Uri uri,
            @Nullable String[] projection,
            @Nullable String selection,
            @Nullable String[] selectionArgs,
            @Nullable String sortOrder
    ) {
        if (RSLog.DEBUG) {
            StringBuilder sb = new StringBuilder();
            sb.append("query: uri = " + uri);
            sb.append(", projection = {");
            if (projection != null) {
                for (String proj : projection) {
                    sb.append("[" + proj + "]");
                }
            }
            sb.append("}, selection = " + selection);
            sb.append(", selectionArgs = {");
            if (selectionArgs != null) {
                for (String args : selectionArgs) {
                    sb.append("[" + args + "]");
                }
            }
            sb.append("}, sortOrder = " + sortOrder);
            RSLog.d("query: " + sb.toString());
        }

        checkUriOrThrows(uri);

        if (sqliteHelper == null) {
            RSLog.e("query() error, the database not exist, " + uri);
            return null;
        }

        SQLiteDatabase db = sqliteHelper.getReadableDatabase();

        Cursor cursor = db.query(
                uri.getPathSegments().get(0),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );

        cursor.setNotificationUri(
                getContext().getContentResolver(),
                uri
        );

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        if (RSLog.DEBUG) RSLog.d("insert: " + uri.toString());

        checkUriOrThrows(uri);

        if (sqliteHelper == null) {
            RSLog.e("insert() error, the database not exist, " + uri);
            return null;
        }

        SQLiteDatabase db = sqliteHelper.getReadableDatabase();

        final long rowId = db.insertOrThrow(
                uri.getPathSegments().get(0),
                null,
                values
        );

        Uri resultUri;
        if (rowId > 0) {
            resultUri = ContentUris.withAppendedId(uri, rowId);
            notifyChange(resultUri);
        } else {
            RSLog.w("insert(): insert fail, uri =" + uri + ", rowId = " + rowId);
            resultUri =  null;
        }

        return resultUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        if(RSLog.DEBUG) {
            StringBuilder sb = new StringBuilder();
            sb.append("uri = " + uri.toString());
            sb.append(", selection = " + selection);
            sb.append(", selectionArgs = {");
            if (selectionArgs != null) {
                for (int i = 0; i < selectionArgs.length; i++) {
                    sb.append("[" + selectionArgs[i] + "],");
                }
            }
            sb.append("}");
            RSLog.d("delete: " + sb.toString());
        }

        checkUriOrThrows(uri);

        if (sqliteHelper == null) {
            RSLog.e("delete() error, the database not exist, " + uri);
            return -1;
        }

        SQLiteDatabase db = sqliteHelper.getReadableDatabase();

        final int count = db.delete(
                uri.getPathSegments().get(0),
                selection,
                selectionArgs
        );

        notifyChange(uri);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        if(RSLog.DEBUG) {
            StringBuilder sb = new StringBuilder();
            sb.append("uri = " + uri.toString());
            sb.append(", selection = " + selection);
            sb.append(", whereArgs = {");
            if (selectionArgs != null) {
                for (int i = 0; i < selectionArgs.length; i++) {
                    sb.append("[" + selectionArgs[i] + "],");
                }
            }
            sb.append("}");
            RSLog.d("update: " + sb.toString());
        }

        checkUriOrThrows(uri);

        if (sqliteHelper == null) {
            RSLog.e("update() error, the database not exist, "+uri);
            return -1;
        }

        SQLiteDatabase db = sqliteHelper.getReadableDatabase();

        final int count = db.update(
                uri.getPathSegments().get(0),
                values,
                selection,
                selectionArgs
        );

        return count;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int code = uriMatcher.match(uri);
        for (int i = 0; i < mimeTypes.size(); ++i) {
            if (code == mimeTypes.keyAt(i)) {
                return mimeTypes.valueAt(i);
            }
        }
        throw new IllegalArgumentException("Unknown URI " + uri);
    }

    protected static String createAuthority(String packageName, String providerName) {
        return String.format(
                CONTENT_PROVIDER_AUTHORITY_FORMAT,
                packageName,
                providerName
        );
    }

    protected static Uri createContentUri(String authority, String providerPath) {
        return Uri.parse(
                String.format(
                        CONTENT_PROVIDER_URI_FORMAT,
                        authority,
                        providerPath
                )
        );
    }

    protected static int createDirCode(int ordinal) {
        return ordinal * 10;
    }

    protected static int createItemCode(int ordinal) {
        return ordinal * 10 + 1;
    }

    protected static String createMineSubtype(String vendorName, String mineSubtype) {
        // vnd.(vendor name).(sub mine type)
        return String.format(
                MIME_SUBTYPE_FORMAT,
                vendorName,
                mineSubtype
        );
    }

    protected static String createDirMineType(String vendorName, String subtypeName) {
        // (core mine type)/(sub mine type)
        return String.format(
                MIME_TYPE_FORMAT,
                DIR_CORE_MINE_TYPE,
                createMineSubtype(vendorName, subtypeName)
        );
    }

    protected static String createItemMineType(String vendorName, String subtypeName) {
        // (core mine type)/(sub mine type)
        return String.format(
                MIME_TYPE_FORMAT,
                ITEM_CORE_MINE_TYPE,
                createMineSubtype(vendorName, subtypeName)
        );
    }

    protected static List<String> createColumns(String ... columns) {
        return Collections.unmodifiableList(Arrays.asList(columns));
    }

    protected static String createTable(String name) {
        return name.toLowerCase(Locale.US);
    }

    protected void checkUriOrThrows(Uri uri) {
        getType(uri);
    }

    protected void notifyChange(Uri ... uris) {
        if (uris == null) {
            return;
        }
        if (RSLog.DEBUG) {
            RSLog.v("notifyChange: " + uris.toString());
        }
        Context context = getContext();
        if (context != null) {
            ContentResolver contentResolver = context.getContentResolver();

            for (Uri uri : uris) {
                contentResolver.notifyChange(uri, null);
            }
        }
    }


}
