package com.rayshen.applib.providers;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Shu-Hua Shen on 2018/3/26.
 */

public abstract class RSBaseSQLiteContentProvider extends RSBaseContentProvider {

    protected RSBaseSQLiteOpenHelper sqliteHelper;

    protected abstract RSBaseSQLiteOpenHelper createSQLiteHelper();

    @Override
    public boolean onCreate() {
        boolean isReady = super.onCreate();
        sqliteHelper = createSQLiteHelper();
        return isReady;
    }

    protected static List<String> createColumns(String ... columns) {
        return Collections.unmodifiableList(Arrays.asList(columns));
    }

    protected static String createTable(String name) {
        return name.toLowerCase(Locale.US);
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        checkUriOrThrows(uri);

        SQLiteDatabase db = sqliteHelper.getReadableDatabase();

        if (db == null) {
            return null;
        }

        Cursor cursor = db.query(
                uri.getPathSegments().get(0),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        {
            cursor.setNotificationUri(
                    getContext().getContentResolver(),
                    uri
            );
        }

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        checkUriOrThrows(uri);

        SQLiteDatabase db = sqliteHelper.getWritableDatabase();

        final long rowId = db.insertOrThrow(
                uri.getPathSegments().get(0),
                null,
                values
        );

        Uri returnUri = ContentUris.withAppendedId(uri, rowId);

        notifyChangeToAll(uri, returnUri);
        return returnUri;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        checkUriOrThrows(uri);

        SQLiteDatabase db = sqliteHelper.getWritableDatabase();

        final int count = db.update(
                uri.getPathSegments().get(0),
                values,
                selection,
                selectionArgs
        );
        notifyChangeToAll(uri);
        return count;
    }
    
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        checkUriOrThrows(uri);

        SQLiteDatabase db = sqliteHelper.getWritableDatabase();

        final int count = db.delete(
                uri.getPathSegments().get(0),
                selection,
                selectionArgs
        );
        notifyChangeToAll(uri);
        return count;
    }

    /**
     * If _id is specified in Uri, concatenate it with selection and return it.
     *
     * @param uri Uri
     * @param baseSelection
     * @return selection with _id condition concatenated
     */
    protected String appendSelection(Uri uri, String baseSelection) {
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 1) {
            return baseSelection;
        }

        String only_IDColumnString = String.format(
                "`%s` = ?",
                BaseColumns._ID
        );

        if (baseSelection == null) {
            return only_IDColumnString;
        }

        return String.format(
                "%s AND (%s)",
                only_IDColumnString,
                baseSelection
        );
    }

    /**
     * If _id is specified by Uri, concatenate it with selectionArgs and return it.
     *
     * @param uri Uri
     * @param baseSelectionArgs
     * @return selectionArgs with _id condition concatenated
     */
    protected String[] appendSelectionArgs(Uri uri, String[] baseSelectionArgs) {
        if (uri == null) {
            return baseSelectionArgs;
        }

        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 1) {
            return baseSelectionArgs;
        }

        if (baseSelectionArgs == null || baseSelectionArgs.length == 0) {
            return new String[] {pathSegments.get(1)};
        }

        String[] appendedArgs = new String[baseSelectionArgs.length + 1];
        {
            appendedArgs[0] = pathSegments.get(1);
            System.arraycopy(
                    baseSelectionArgs,
                    0,
                    appendedArgs,
                    1,
                    baseSelectionArgs.length
            );
        }
        return appendedArgs;
    }

    protected void notifyChangeToAll(Uri ... uris) {
        Context context = getContext();
        if (context != null) {
            ContentResolver contentResolver = context.getContentResolver();

            for (Uri uri : uris) {
                contentResolver.notifyChange(uri, null);
            }
        }
    }
}
