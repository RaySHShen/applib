package com.rayshen.applib.providers;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by Shu-Hua Shen on 2018/3/26.
 */

public abstract class RSBaseSQLiteOpenHelper extends SQLiteOpenHelper {

    protected static final String DROP_TABLE_SQL_FORMAT = "DROP TABLE IF EXISTS `%s`";

    @NonNull
    protected abstract List<String> getSQLsToCreateTable();

    @NonNull
    protected abstract List<String> getAllTableNames();

    public RSBaseSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        this(context, name, factory, version, null);
    }

    public RSBaseSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            for (String sql : getSQLsToCreateTable()) {
                db.execSQL(sql);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAllTables(db);
        onCreate(db);
    }

    protected void dropAllTables(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            for (String tableName : getAllTableNames()) {
                String sql = String.format(DROP_TABLE_SQL_FORMAT, tableName);
                db.execSQL(sql);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }
}
