package com.rayshen.applib.permission;

/**
 * Created by Shu-Hua Shen on 2017/7/13.
 */
public interface OnPermissionCallback {
    void onRequestAllow(String permissionName);
    void onRequestRefuse(String permissionName);
    void onRequestNoAsk(String permissionName);
}
