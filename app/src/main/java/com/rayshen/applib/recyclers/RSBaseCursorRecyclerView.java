package com.rayshen.applib.recyclers;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.rayshen.applib.recyclers.base.RSBaseRecyclerView;

/**
 * Created by Shu-Hua Shen on 2018/3/24.
 */

public abstract class RSBaseCursorRecyclerView extends RSBaseRecyclerView
{

    public RSBaseCursorRecyclerView(Context context) {
        this(context, null);
    }

    public RSBaseCursorRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RSBaseCursorRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void initialize(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super.initialize(context, attrs, defStyle);
    }

    /**
     *Update the display as soon as you populate the data set
     *
     * @param cursor Cursor for DataSet.
     * @return for method chain.
     */
    public RSBaseCursorRecyclerView setDataSet(Cursor cursor) {
        RSBaseCursorAdapter adapter = (RSBaseCursorAdapter) getAdapter();
        if (adapter != null) {
            adapter.swapCursor(cursor);
        }

        return this;
    }

    /**
     * Derived RecyclerView.Adapter for Cursor type.
     */
    protected abstract class RSBaseCursorAdapter<VH extends RSBaseViewHolder<RSBaseItem>> extends RSBaseAdapter<VH>
    {
        protected static final int INVALID_RAW_ID = -1;

        @Nullable
        protected Cursor cursor = null;
        protected int rawIDColumn = INVALID_RAW_ID;
        protected boolean dataValid;

        protected ContentObserver contentObserver;
        protected DataSetObserver dataSetObserver;

        protected final DataSetObservable dataSetObservable = new DataSetObservable();

        /**
         * Get item information from cursor.
         *
         * @param cursor Cursor.
         * @return Instance of item information.
         */
        protected abstract RSBaseItem getItem(Cursor cursor);

        protected abstract String primaryKey();

        /**
         * Constructor.
         */
        public RSBaseCursorAdapter() {
            super();
            initialize();
        }

        /**
         * Initialize on Constructor.
         */
        protected void initialize() {
            cursor = null;

            contentObserver = createContentObserver();
            dataSetObserver = createCursorDataSetObserver();

            setupCursor(cursor);

            this.setHasStableIds(true);
        }

        @Override
        public void onBindViewHolder(VH viewHolder, int position) {
            if (!dataValid || cursor == null) {
                throw new IllegalStateException("this should only be called when the cursor id valid.");
            }

            // Determine case of header and footer
            int itemPosition = itemPosition(position);
            if (itemPosition < 0 || cursor.getCount() <= itemPosition) {
                return;
            }

            if (!cursor.moveToPosition(itemPosition)) {
                throw new IllegalStateException("couldn't move cursor to position " + position);
            }

            /**
             * Data extracted from cursor is added to viewHolder's view
             * For each view, set items converted from cursor as necessary
             */
            RSBaseCursorViewHolder cursorViewHolder = (RSBaseCursorViewHolder) viewHolder;
            {
                RSBaseItem item = getItem(cursor);
                cursorViewHolder.bindItem(item);
            }
        }

        /**
         * get id for each item.
         *
         * @param position item position.
         * @return rawId of each item.
         */
        @Override
        public long getItemId(int position) {
            if (dataValid) {
                if (cursor != null) {
                    int itemPosition = itemPosition(position);
                    if (itemPosition != NO_POSITION) {
                        if (cursor.moveToPosition(itemPosition)) {
                            return cursor.getLong(rawIDColumn);
                        }
                    }
                }
            }
            return super.getItemId(position);
        }

        /**
         * Set up cursor.
         *
         * @param cursor Cursor.
         */
        public void setupCursor(Cursor cursor) {
            dataValid = cursor != null;

            rawIDColumn = dataValid
                    ? cursor.getColumnIndexOrThrow(primaryKey())
                    : INVALID_RAW_ID;

            if (dataValid) {
                cursor.registerContentObserver(contentObserver);
                cursor.registerDataSetObserver(dataSetObserver);
            }
        }

        /**
         * tear down cursor.
         *
         * @param cursor Cursor.
         */
        public void teardownCursor(Cursor cursor) {
            rawIDColumn = INVALID_RAW_ID;

            if (cursor != null) {
                cursor.unregisterContentObserver(contentObserver);
                cursor.unregisterDataSetObserver(dataSetObserver);
            }
        }

        /**
         * SwapCursor of own implementation.
         *
         * @param newCursor new cursor for updating.
         * @return oldCursor.
         */
        public Cursor swapCursor(Cursor newCursor) {
            if (newCursor == cursor) {
                return null;
            }

            Cursor oldCursor = cursor;
            teardownCursor(oldCursor);

            setupCursor(newCursor);
            cursor = newCursor;

            notifyDataSetChanged();

            return oldCursor;
        }

        /**
         * Get cursor.
         *
         * @return Cursor.
         */
        public Cursor getCursor() {
            return cursor;
        }

        protected RSCursorContentObserver createContentObserver() {
            return new RSCursorContentObserver();
        }

        protected RSCursorDataSetObserver createCursorDataSetObserver() {
            return new RSCursorDataSetObserver();
        }

        /**
         * Total number of items.
         *
         * @return total item count.
         */
        @Override
        public int getItemCount() {
            if (!dataValid) {
                return 0;
            }

            int count = cursor.getCount();
            if (count == 0) {
                return 0;
            }

            return count
                    + (hasHeaderView() ? 1 : 0)
                    + (hasFooterView() ? 1 : 0)
                    ;
        }

        protected void onContentChanged() {
            // TODO: cursor changed
        }

        /**
         * Method for linking DataSetObserver
         * Conditioned to DataSetObservable
         */
        protected void notifyDataSetInvalidated() {
            dataSetObservable.notifyInvalidated();
        }

        protected class RSCursorContentObserver extends ContentObserver {
            public RSCursorContentObserver() {
                super(new Handler());
            }

            @Override
            public boolean deliverSelfNotifications() {
                return true;
            }

            @Override
            public void onChange(boolean selfChange) {
                onContentChanged();
            }
        }

        protected class RSCursorDataSetObserver extends DataSetObserver {
            @Override
            public void onChanged() {
                dataValid = true;
                notifyDataSetChanged();
            }

            @Override
            public void onInvalidated() {
                dataValid = false;
                notifyDataSetInvalidated();
            }
        }
    }

    /**
     * @param <I> Item Information.
     */
    protected abstract class RSBaseCursorViewHolder<I extends RSBaseItem> extends RSBaseViewHolder<I> {
        /**
         * Constructor.
         *
         * @param itemView Outer view inflated.
         */
        public RSBaseCursorViewHolder(View itemView) {
            super(itemView);
        }
    }

    protected static abstract class RSBaseCursorItem extends RSBaseItem {
        public RSBaseCursorItem(Cursor cursor) {
            super();
        }
    }
}
