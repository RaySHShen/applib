package com.rayshen.applib.recyclers;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.rayshen.applib.recyclers.base.RSBaseRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shu-Hua Shen on 2018/3/24.
 */

public abstract class RSBaseArrayRecyclerView extends RSBaseRecyclerView
{

    protected List<? extends RSBaseItem> items = new ArrayList<>();

    public RSBaseArrayRecyclerView(Context context) {
        this(context, null);
    }

    public RSBaseArrayRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RSBaseArrayRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * @param items DataSet.
     * @return for method chain.
     */
    public RSBaseArrayRecyclerView setDataSet(List<RSBaseItem> items) {
        return setDataSet(items, true);
    }

    /**
     * Update the display as soon as you populate the data set
     *
     * @param items DataSet.
     * @return for method chain.
     */
    public RSBaseArrayRecyclerView setDataSet(List<RSBaseItem> items, boolean notify) {
        this.items = items;
        if (notify) {
            Adapter adapter = getAdapter();
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
        return this;
    }

    /**
     * Derived RecyclerView.Adapter for Array type.
     */
    protected abstract class RSBaseArrayAdapter<VH extends RSBaseViewHolder<RSBaseItem>> extends RSBaseAdapter<VH>
    {
        @Override
        public int getItemCount() {
            if (items == null) {
                return 0;
            }

            return items.size()
                    + (hasHeaderView() ? 1 : 0)
                    + (hasFooterView() ? 1 : 0)
                    ;
        }

        /**
         * Get item information of display position
         *
         * @param notifyPosition
         * @return Instance of RSBaseItem.
         */
        protected RSBaseItem getItem(int notifyPosition) {
            if (items == null) {
                return null;
            }

            int itemPosition = itemPosition(notifyPosition);

            if (itemPosition == NO_POSITION || items.size() <= itemPosition) {
                return null;
            }
            return items.get(itemPosition);
        }

        /**
         * Return the position of the item in the array
         *
         * @param item
         * @return index.
         */
        protected int indexOf(KLBaseArrayItem item) {
            return items.indexOf(item);
        }
    }

    /**
     *
     * @param <I> Item Information.
     */
    protected abstract class KLBaseArrayViewHolder<I extends RSBaseItem> extends RSBaseViewHolder<I> {
        /**
         * Constructor.
         *
         * @param itemView Outer view inflated.
         */
        public KLBaseArrayViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * Base item class  of RecyclerView (origin)
     */
    public static abstract class KLBaseArrayItem extends RSBaseItem {
    }
}
