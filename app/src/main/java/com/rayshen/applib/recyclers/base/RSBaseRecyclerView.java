package com.rayshen.applib.recyclers.base;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shu-Hua Shen on 2018/3/24.
 */

public abstract class RSBaseRecyclerView extends RecyclerView {

    public static final int HEADER_VIEW_TYPE = -1;
    public static final int FOOTER_VIEW_TYPE = -2;

    public static final String SAVE_BUNDLE_KEY_PARENT = "save_bundle_key_parent";
    public static final String SAVE_BUNDLE_KEY_ORIENTATION = "save_bundle_key_orientation";

    protected View emptyView;
    protected View headerView;
    protected View footerView;

    protected OnClickItemCallbacks onClickItemCallbacks = null;
    protected OnSwipeListener      onSwipeListener      = null;

    protected List<RSScrollStatusObserver> scrollStatusObservers = new ArrayList<>();

    /**
     * @return Instance of derived ItemDecoration class
     */
    @Nullable
    protected abstract RecyclerView.ItemDecoration createItemDecoration(Context context);

    /**
     * @return Instance of derived LayoutManager class.
     */
    @Nullable
    protected abstract RecyclerView.LayoutManager createLayoutManager(Context context);

    /**
     * @return Instance of derived LayoutManager class.
     */
    @Nullable
    protected abstract RecyclerView.Adapter createAdapter(Context context);

    protected enum SWIPE_STATUS {
        SCROLL,
        SWIPE,
        NONE,
    }

    public interface OnClickItemCallbacks {
        void onClickItem(
                RSBaseRecyclerView recyclerView,
                @Nullable View view,
                int notifyPosition,
                RSBaseItem item
        );

        boolean onLongClickItem(
                RSBaseRecyclerView recyclerView,
                @Nullable View view,
                int notifyPosition,
                RSBaseItem item
        );

        void onClickInnerView(
                RSBaseRecyclerView recyclerView,
                @Nullable View view,
                int notifyPosition,
                RSBaseItem item
        );
    }

    public interface OnSwipeListener {
        void onSnapped(
                RSBaseRecyclerView recyclerView,
                @Nullable RSBaseItem item,
                int snappedIndex,
                List<Float> snapValues
        );
    }

    public interface RSSwipeableViewInterface {
        View getSwipeTargetView();
        void resetSwipeTargetView();
    }

    public RSBaseRecyclerView(Context context) {
        this(context, null);
    }

    public RSBaseRecyclerView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RSBaseRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize(context, attrs, defStyle);
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable state = super.onSaveInstanceState();

        if (isNotOrientableLayoutManager()) {
            return state;
        }

        int orientation = ((LinearLayoutManager) getLayoutManager()).getOrientation();

        Bundle bundle = new Bundle();
        bundle.putParcelable(SAVE_BUNDLE_KEY_PARENT, state);
        bundle.putInt(SAVE_BUNDLE_KEY_ORIENTATION, orientation);

        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (isNotOrientableLayoutManager() || !(state instanceof Bundle)) {
            super.onRestoreInstanceState(state);
            return;
        }

        Bundle bundle = (Bundle) state;
        super.onRestoreInstanceState(bundle.getParcelable(SAVE_BUNDLE_KEY_PARENT));

        ((LinearLayoutManager) getLayoutManager()).setOrientation(bundle.getInt(SAVE_BUNDLE_KEY_ORIENTATION));
    }

    /**
     * If needed, override and replace return instance.
     *
     * @return Instance of derived DefaultItemAnimator class.
     */
    @Nullable
    protected RecyclerView.ItemAnimator createItemAnimator(Context context) {
        return new DefaultItemAnimator();
    }

    @Nullable
    protected RecyclerView.OnScrollListener createScrollListener() {
        return new RSScrollListener();
    }

    @NonNull
    protected AdapterDataObserver emptyViewAdapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            viewEmptyView();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            viewEmptyView();
        }

        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            viewEmptyView();
        }
    };
    
    /**
     * Initialize on constructor.
     *
     * @param context  Context.
     * @param attrs    Attribute in XML.
     * @param defStyle defined style.
     */
    protected void initialize(Context context, @Nullable AttributeSet attrs, int defStyle) {
        LayoutManager layoutManager = createLayoutManager(context);
        if (layoutManager != null) {
            setLayoutManager(layoutManager);
        }

        Adapter adapter = createAdapter(context);
        if (adapter != null) {
            setAdapter(adapter);
        }

        ItemAnimator itemAnimator = createItemAnimator(context);
        if (itemAnimator != null) {
            setItemAnimator(itemAnimator);
        }

        ItemDecoration itemDecoration = createItemDecoration(context);
        if (itemDecoration != null) {
            addItemDecoration(itemDecoration);
        }

        OnScrollListener scrollListener = createScrollListener();
        if (scrollListener != null) {
            addOnScrollListener(scrollListener);
        }
    }

    /**
     * Does LayoutManager has a property of orientation ?
     * GridLayoutManager is derived class of LinearLayoutManager.
     */
    protected Boolean isOrientableLayoutManager() {
        return getLayoutManager() instanceof LinearLayoutManager;
    }

    protected Boolean isNotOrientableLayoutManager() {
        return !isOrientableLayoutManager();
    }

    public RSBaseRecyclerView refresh() {
        setAdapter(getAdapter());
        return this;
    }

    /**
     * Has EmptyView.
     *
     * @return true: has.
     */
    protected boolean hasEmptyView() {
        return this.emptyView != null;
    }

    /**
     * Has headerView.
     *
     * @return true: has.
     */
    protected boolean hasHeaderView() {
        return this.headerView != null;
    }

    /**
     * Has footerView.
     *
     * @return true: has.
     */
    protected boolean hasFooterView() {
        return footerView != null;
    }

    /**
     *Install EmptyView for RecyclerView
     *
     * @param emptyView Empty View.
     * @return for method chain.
     */
    public RSBaseRecyclerView setupEmptyView(View emptyView) {
        this.emptyView = emptyView;

        Adapter adapter = getAdapter();
        if (adapter != null) {
            if (hasEmptyView()) {
                adapter.registerAdapterDataObserver(emptyViewAdapterDataObserver);
            } else {
                adapter.unregisterAdapterDataObserver(emptyViewAdapterDataObserver);
            }
        }

        return this;
    }

    public RSBaseRecyclerView setupEmptyView(int viewId) {
        View emptyView = null;

        View rootView = getRootView();
        if (rootView != null) {
            emptyView = rootView.findViewById(viewId);
        }

        setupEmptyView(emptyView);

        return this;
    }

    /**
     * Get the total number of excluding Header and Footer
     *
     * @return raw item count.
     */
    public int getRawItemCount() {
        Adapter adapter = getAdapter();
        if (adapter == null) {
            return 0;
        }
        int count = adapter.getItemCount();
        int offset = 0;

        if (hasHeaderView()) {
            ++offset;
        }

        if (hasFooterView()) {
            ++offset;
        }
        if (count <= offset) {
            return 0;
        }
        return count - offset;
    }

    /**
     * get notify position by item position.
     */
    public int notifyPosition(int itemPosition) {
        if (itemPosition == NO_POSITION) {
            return NO_POSITION;
        }

        return hasHeaderView()
                ? itemPosition + 1
                : itemPosition
                ;
    }

    /**
     * Get the actual position of notify position
     *
     * @param notifyPosition Position calculated from header and footer
     *
     * @return raw position in array items.
     */
    public int itemPosition(int notifyPosition) {
        if (notifyPosition == NO_POSITION) {
            return NO_POSITION;
        }

        int itemPosition = notifyPosition;
        if (hasHeaderView()) {
            if (itemPosition == 0) {
                return NO_POSITION;
            }

            --itemPosition;
        }

        if (hasFooterView()) {
            if (getRawItemCount() <= itemPosition) {
                return NO_POSITION;
            }
        }

        return itemPosition;
    }

    protected void viewEmptyView() {
        boolean isEmpty = getRawItemCount() == 0;

        int contentVisibility =
                isEmpty
                        ? View.GONE
                        : View.VISIBLE
                ;

        if (hasHeaderView()) {
            headerView.setVisibility(contentVisibility);
        }

        if (hasFooterView()) {
            footerView.setVisibility(contentVisibility);
        }

        if (hasEmptyView()) {
            emptyView.setVisibility(
                    isEmpty
                            ? View.VISIBLE
                            : View.GONE
            );
        }
    }

    public RSBaseRecyclerView setHeaderView(View headerView) {
        headerView = headerView;

        return this;
    }

    public RSBaseRecyclerView setFooterView(View footerView) {
        footerView = footerView;
        return this;
    }

    protected void onItemClicked(@Nullable View view, int notifyPosition, @Nullable RSBaseItem item) {
        if (onClickItemCallbacks != null) {
            onClickItemCallbacks.onClickItem(
                    this,
                    view,
                    notifyPosition,
                    item
            );
        }
    }

    protected boolean onItemLongClicked(@Nullable View view, int notifyPosition, @Nullable RSBaseItem item)
    {

        return onClickItemCallbacks != null
                && onClickItemCallbacks.onLongClickItem(this, view, notifyPosition, item);

    }

    protected void onInnerViewClicked(@Nullable View view, int notifyPosition, @Nullable RSBaseItem item) {
        if (onClickItemCallbacks != null) {
            onClickItemCallbacks.onClickInnerView(
                    this,
                    view,
                    notifyPosition,
                    item
            );
        }
    }

    public RSBaseRecyclerView setOnClickItemCallbacks(OnClickItemCallbacks callbacks) {
        onClickItemCallbacks = callbacks;
        return this;
    }

    public RSBaseRecyclerView setOnSwipeListener(OnSwipeListener listener) {
        onSwipeListener = listener;
        return this;
    }

    public RSBaseRecyclerView addScrollStatusObserver(RSScrollStatusObserver observer) {
        scrollStatusObservers.add(observer);
        return this;
    }

    public RSBaseRecyclerView removeScrollStatusObserver(RSScrollStatusObserver observer) {
        scrollStatusObservers.remove(observer);
        return this;
    }

    protected double swipeSensitivity() {
        return 0.4;
    }

    /**
     * Derived RecyclerView.Adapter.
     */
    protected abstract class RSBaseAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH>
    {
        protected View inflateView(RecyclerView recyclerView, int viewType) {
            switch (viewType) {
                case HEADER_VIEW_TYPE:
                    return headerView;

                case FOOTER_VIEW_TYPE:
                    return footerView;

                default:
                    return null;
            }
        }

        protected boolean isViewTypeBodyView(int viewType) {
            return viewType != HEADER_VIEW_TYPE
                    && viewType != FOOTER_VIEW_TYPE;
        }

        @Override
        public int getItemViewType(int position) {
            if (hasHeaderView()) {
                if (position == 0) {
                    return HEADER_VIEW_TYPE;
                }
            }

            if (hasFooterView()) {
                if (position == getItemCount() - 1) {
                    return FOOTER_VIEW_TYPE;
                }
            }

            return super.getItemViewType(position);
        }
    }

    protected abstract class RSBaseViewHolder<I extends RSBaseItem> extends RecyclerView.ViewHolder
            implements OnClickListener, OnLongClickListener
    {

        protected I item;

        public int snappedIndex = -1;

        public RSBaseViewHolder(View itemView) {
            super(itemView);

            if (this.itemView != null) {
                /**
                 * Find views.
                 */
                holdViews();

                /**
                 * Set lister into views.
                 */
                setupViews();

                /**
                 * Set this ViewHolder as the relay onClickListener
                 */
                this.itemView.setOnClickListener(this);
                this.itemView.setOnLongClickListener(this);
            }
        }

        /**
         * RecyclerView common onClickItem processing
         */
        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            if (view == itemView) {
                onItemClicked(view, adapterPosition, item);
            } else {
                onInnerViewClicked(view, adapterPosition, item);
            }
        }

        /**
         * RecyclerView common onLongClickItem processing
         */
        @Override
        public boolean onLongClick(View view) {
            return onItemLongClicked(view, getAdapterPosition(), item);
        }

        protected abstract void holdViews();

        protected abstract void setupViews();

        public void bindItem(@Nullable I item) {
            this.item = item;
        }
    }

    public static abstract class RSBaseItem {
    }

    /**
     * ItemDecoration to draw divider for simple list RecyclerView.
     */
    protected static class RSDividerItemDecoration extends RecyclerView.ItemDecoration {
        protected static final int[] DEFAULT_ATTRIBUTES = new int[]
        {
                android.R.attr.listDivider,
        };

        protected Drawable dividerDrawable;

        public RSDividerItemDecoration(final Context context) {
            final TypedArray typedArray = context.obtainStyledAttributes(DEFAULT_ATTRIBUTES);
            {
                dividerDrawable = typedArray.getDrawable(0);
            }
            typedArray.recycle();
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            if (isEmpty(recyclerView)) {
                return;
            }

            int expandedSpan = dividerDrawable.getIntrinsicHeight();
            int right = 0;
            int bottom = 0;

            if (isVerticalList(recyclerView)) {
                bottom = expandedSpan;
            } else {
                right = expandedSpan;
            }

            outRect.set(0, 0, right, bottom);
        }

        @Override
        public void onDraw(final Canvas canvas, final RecyclerView recyclerView, final RecyclerView.State state) {
            if (isEmpty(recyclerView)) {
                return;
            }
            drawBorder(canvas, recyclerView);
        }

        @Override
        public void onDrawOver(final Canvas canvas, final RecyclerView recyclerView, final RecyclerView.State state) {
        }

        protected boolean isEmpty(RecyclerView recyclerView) {
            if (recyclerView instanceof RSBaseRecyclerView) {
                return ((RSBaseRecyclerView) recyclerView).getRawItemCount() == 0;
            }
            return true;
        }

        protected void drawBorder(final Canvas canvas, final RecyclerView recyclerView) {
            boolean isVerticalList = isVerticalList(recyclerView);
            if (isVerticalList) {
                drawHorizontalBorder(canvas, recyclerView);
            } else {
                drawVerticalBorder(canvas, recyclerView);
            }
        }

        protected void drawHorizontalBorder(final Canvas canvas, final RecyclerView recyclerView) {
            final int left  = recyclerView.getPaddingLeft();
            final int right = recyclerView.getWidth() - recyclerView.getPaddingRight();

            int borderThin = dividerDrawable.getIntrinsicHeight();

            final int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; ++i) {
                final View childView = recyclerView.getChildAt(i);
                final RecyclerView.LayoutParams params =
                        (RecyclerView.LayoutParams) childView.getLayoutParams();
                final int top = childView.getBottom() + params.bottomMargin;
                final int bottom = top + borderThin;

                dividerDrawable.setBounds(left, top, right, bottom);
                dividerDrawable.draw(canvas);
            }
        }

        protected void drawVerticalBorder(final Canvas canvas, final RecyclerView recyclerView) {
            final int top  = recyclerView.getPaddingTop();
            final int bottom = recyclerView.getHeight() - recyclerView.getPaddingBottom();

            int borderThin = dividerDrawable.getIntrinsicHeight();

            final int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; ++i) {
                final View childView = recyclerView.getChildAt(i);
                final RecyclerView.LayoutParams params =
                        (RecyclerView.LayoutParams) childView.getLayoutParams();
                final int left = childView.getRight() + params.rightMargin;
                final int right = left + borderThin;

                dividerDrawable.setBounds(left, top, right, bottom);
                dividerDrawable.draw(canvas);
            }
        }

        protected boolean isVerticalList(RecyclerView recyclerView) {
            LayoutManager manager = recyclerView.getLayoutManager();
            if(manager instanceof StaggeredGridLayoutManager) {
                return ((StaggeredGridLayoutManager) manager).getOrientation() == StaggeredGridLayoutManager.VERTICAL;
            }
            return ((LinearLayoutManager) manager).getOrientation() == LinearLayoutManager.VERTICAL;
        }
    }

    /**
     * ItemDecoration to draw divider for simple list RecyclerView.
     */
    protected static class RSGridItemDecoration extends RecyclerView.ItemDecoration {

        protected static final int[] DEFAULT_ATTRIBUTES = new int[] {android.R.attr.listDivider};

        protected Drawable dividerDrawable;

        public RSGridItemDecoration(final Context context) {
            final TypedArray typedArray = context.obtainStyledAttributes(DEFAULT_ATTRIBUTES);
            dividerDrawable = typedArray.getDrawable(0);
            typedArray.recycle();
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView recyclerView, RecyclerView.State state) {
            if (isEmpty(recyclerView)) {
                return;
            }
            int expandedSpan = dividerDrawable.getIntrinsicHeight() / 2;
            outRect.set(expandedSpan, expandedSpan, expandedSpan, expandedSpan);
        }

        protected int getItemPosition(View itemView) {
            return ((RecyclerView.LayoutParams) itemView.getLayoutParams()).getViewAdapterPosition();
        }

        @Override
        public void onDraw(final Canvas canvas, final RecyclerView recyclerView, final RecyclerView.State state) {
            if (isEmpty(recyclerView)) {
                return;
            }
            drawBorder(canvas, recyclerView);
        }

        @Override
        public void onDrawOver(final Canvas canvas, final RecyclerView recyclerView, final RecyclerView.State state) {
        }

        protected boolean isEmpty(RecyclerView recyclerView) {
            if (recyclerView instanceof RSBaseRecyclerView) {
                return ((RSBaseRecyclerView) recyclerView).getRawItemCount() == 0;
            }
            return true;
        }

        protected void drawBorder(final Canvas canvas, final RecyclerView recyclerView) {
            GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            int gridColumn = gridLayoutManager.getSpanCount();
            if (gridColumn == 0) {
                return;
            }

            int borderThin = dividerDrawable.getIntrinsicHeight() / 2;

            final int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; ++i) {
                final View childView = recyclerView.getChildAt(i);
                final RecyclerView.LayoutParams params =
                        (RecyclerView.LayoutParams) childView.getLayoutParams();

                final int left = childView.getLeft() - params.leftMargin - borderThin;
                final int right = childView.getRight() + params.rightMargin + borderThin;

                final int top = childView.getTop() - params.topMargin - borderThin;
                final int bottom = childView.getBottom() + params.bottomMargin + borderThin;

                dividerDrawable.setBounds(left, top, right, bottom);
                dividerDrawable.draw(canvas);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Inner classes for SwipeableRecyclerView
    ////////////////////////////////////////////////////////////////////////////////////////////////

    protected abstract class RSBaseSwipeableOnItemTouchListener implements RecyclerView.OnItemTouchListener {

        protected float lastPosX = 0f;
        protected float lastPosY = 0f;

        protected float startDownX = 0f;
        protected float startDownY = 0f;

        protected float startViewTransactionX = 0f;
        protected float startViewTransactionY = 0f;

        protected float directionX = 0f;
        protected float directionY = 0f;

        protected boolean swipeWithinSnapStart = false;
        protected boolean swipeWithinSnapEnd   = false;

        protected boolean exclusiveSwipe = false;

        protected SWIPE_STATUS swipeStatus = SWIPE_STATUS.NONE;

        protected RSSwipeableViewInterface swipeableItemView;

        protected abstract List<Float> snapPositions();

        @Override
        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            float posX = motionEvent.getX();
            float posY = motionEvent.getY();

            boolean isVerticalList = isVerticalList();

            View swipeView;

            int action = motionEvent.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    RSSwipeableViewInterface currentItemView =
                            findChildViewUnder(recyclerView, posX, posY);

                    if (exclusiveSwipe && swipeableItemView != null && swipeableItemView != currentItemView) {
                        swipeView = swipeableItemView.getSwipeTargetView();
                        if (swipeView != null) {
                            snapWithAnimation(recyclerView, swipeView);
                        }
                    }

                    swipeableItemView = currentItemView;

                    startDownX = posX;
                    startDownY = posY;

                    if (swipeableItemView != null) {
                        swipeView = swipeableItemView.getSwipeTargetView();
                        if (swipeView != null) {
                            startViewTransactionX = swipeView.getTranslationX();
                            startViewTransactionY = swipeView.getTranslationY();
                        }
                    }

                    lastPosX = posX;
                    lastPosY = posY;

                    break;

                case MotionEvent.ACTION_MOVE:
                    if (swipeStatus == SWIPE_STATUS.SCROLL) {
                        break;
                    }

                    if (swipeableItemView == null) {
                        break;
                    }

                    swipeView = swipeableItemView.getSwipeTargetView();
                    if (swipeView == null) {
                        break;
                    }

                    float dX = posX - startDownX;
                    float dY = posY - startDownY;

                    if (swipeStatus != SWIPE_STATUS.SWIPE) {
                        double swipeSensitivity = swipeSensitivity();
                        boolean isScroll = isVerticalList
                                ? Math.abs(dX) * swipeSensitivity <= Math.abs(dY)
                                : Math.abs(dY) * swipeSensitivity <= Math.abs(dX)
                                ;
                        if (isScroll) {
                            swipeStatus = SWIPE_STATUS.SCROLL;
                            break;
                        }
                    }

                    swipeStatus = SWIPE_STATUS.SWIPE;

                    directionX = posX - lastPosX;
                    directionY = posY - lastPosY;

                    lastPosX = posX;
                    lastPosY = posY;

                    moveWithLimit(recyclerView, swipeView,
                            (isVerticalList
                                    ?  startViewTransactionX + dX
                                    :  startViewTransactionY + dY
                            )
                    );

                    if (isVerticalList) {
                        motionEvent.setLocation(posX, startDownY);
                    } else {
                        motionEvent.setLocation(startDownX, posY);
                    }

                    break;

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                default:
                    boolean endFromSwipe = swipeStatus == SWIPE_STATUS.SWIPE;

                    swipeStatus = SWIPE_STATUS.NONE;

                    if (endFromSwipe) {
                        if (swipeableItemView == null) {
                            break;
                        }
                        swipeView = swipeableItemView.getSwipeTargetView();

                        onSnap(recyclerView, swipeView,
                                isVerticalList
                                        ? directionX
                                        : directionY
                        );
                        return true;
                    }
                    break;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

        protected RSSwipeableViewInterface findChildViewUnder(RecyclerView recyclerView, float x, float y) {
            return (RSSwipeableViewInterface) recyclerView.findChildViewUnder(x, y);
        }

        protected void onSnap(RecyclerView recyclerView, View snapTargetView, float d) {
            if (recyclerView == null || snapTargetView == null) {
                return;
            }

            int targetSnapIndex;
            if (isVerticalList()) {
                targetSnapIndex = getNearestSnapIndex(
                        recyclerView.getWidth(),
                        snapTargetView.getTranslationX(),
                        d < 0f
                );
            } else {
                targetSnapIndex = getNearestSnapIndex(
                        recyclerView.getHeight(),
                        snapTargetView.getTranslationY(),
                        d < 0f
                );
            }

            snapWithAnimation(recyclerView, snapTargetView, targetSnapIndex);
        }

        protected int getNearestSnapIndex(int span, float pos, boolean orientation) {
            List<Float> snapPositions = snapPositions();
            int maxIndex = snapPositions.size() - 1;
            float currentRatio = pos / span;

            if (orientation) {
                for (int i = maxIndex; 0 <= i; --i) {
                    if (snapPositions.get(i) < currentRatio) {
                        return i;
                    }
                }
                return 0;
            } else {
                for (int i = 0; i <= maxIndex; ++i) {
                    if (currentRatio < snapPositions.get(i)) {
                        return i;
                    }
                }

                return maxIndex;
            }
        }

        protected void moveWithLimit(RecyclerView recyclerView, View snapTargetView, float newPos) {
            boolean isVerticalList = isVerticalList();
            List<Float> snapPositions = snapPositions();

            int span = isVerticalList
                    ? recyclerView.getWidth()
                    : recyclerView.getHeight()
                    ;

            if (swipeWithinSnapStart) {
                newPos = Math.max(
                        newPos,
                        snapPositions.get(0) * span
                );
            }

            if (swipeWithinSnapEnd) {
                newPos = Math.min(
                        newPos,
                        snapPositions.get(snapPositions.size() - 1) * span
                );
            }

            if (isVerticalList) {
                snapTargetView.setTranslationX(newPos);
            } else {
                snapTargetView.setTranslationY(newPos);
            }
        }

        public void snapWithAnimation(RecyclerView recyclerView, View snapTargetView) {
            snapWithAnimation(recyclerView, snapTargetView, -1);
        }

        public void snapWithAnimation(RecyclerView recyclerView, View snapTargetView, int targetSnapIndex) {
            boolean isVertical = isVerticalList();
            int viewSpan = isVertical
                    ? recyclerView.getWidth()
                    : recyclerView.getHeight()
                    ;

            float currentSnap = isVertical
                    ? snapTargetView.getTranslationX()
                    : snapTargetView.getTranslationY()
                    ;

            float targetSnap = targetSnapIndex == -1
                    ? 0f
                    : viewSpan * snapPositions().get(targetSnapIndex)
                    ;

            RSBaseViewHolder holder = (RSBaseViewHolder) findContainingViewHolder(snapTargetView);
            if (holder != null) {
                holder.snappedIndex = targetSnapIndex;
            }

            ObjectAnimator animator;
            if (isVertical) {
                animator = ObjectAnimator
                        .ofFloat(
                                snapTargetView,
                                "translationX",
                                currentSnap,
                                targetSnap
                        )
                ;
            } else {
                animator = ObjectAnimator
                        .ofFloat(
                                snapTargetView,
                                "translationY",
                                currentSnap,
                                targetSnap
                        )
                ;
            }

            animator.addListener(new RSOnSnapListener());
            animator
                    .setDuration(100)
                    .start()
            ;
        }

        public boolean isVerticalList() {
            return true;
        }

        protected class RSOnSnapListener implements Animator.AnimatorListener {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!(animator instanceof ObjectAnimator)) {
                    return;
                }

                Object target = ((ObjectAnimator) animator).getTarget();
                if (!(target instanceof View)) {
                    return;
                }

                View view = (View) target;
                RSBaseViewHolder holder = (RSBaseViewHolder) findContainingViewHolder(view);
                if (holder != null) {
                    if (onSwipeListener != null) {
                        onSwipeListener.onSnapped(
                                RSBaseRecyclerView.this,
                                holder.item,
                                holder.snappedIndex,
                                snapPositions()
                        );
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }
    }

    protected class RSScrollListener extends RecyclerView.OnScrollListener {
        protected int _dx = 0;
        protected int _dy = 0;

        protected int lastHeadPosition = NO_POSITION;
        protected int lastTailPosition = NO_POSITION;

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            int totalItemCount = recyclerView.getAdapter().getItemCount();
            switch (newState) {
                case SCROLL_STATE_DRAGGING:
                    break;
                case SCROLL_STATE_SETTLING:
                    break;
                case SCROLL_STATE_IDLE:
                    _dx = 0;
                    _dy = 0;
                    break;
            }
        }

        /**
         * Callback method to be invoked when the RecyclerView has been scrolled. This will be
         * called after the scroll has completed.
         * <p>
         * This callback will also be called if visible item range changes after a layout
         * calculation. In that case, dx and dy will be 0.
         *      findFirstVisibleItemPosition
         *      findFirstCompletelyVisibleItemPosition
         *      findLastVisibleItemPosition
         *      findLastCompletelyVisibleItemPosition
         *
         *
         * @param recyclerView The RecyclerView which scrolled.
         * @param dx The amount of horizontal scroll.
         * @param dy The amount of vertical scroll.
         */
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            LayoutManager manager = recyclerView.getLayoutManager();
            if (!(manager instanceof LinearLayoutManager)) {
                return;
            }
            LinearLayoutManager theManager = (LinearLayoutManager) manager;

            boolean isVertical = theManager.getOrientation() == LinearLayoutManager.VERTICAL;

            boolean fromIdle = isVertical
                    ? _dy == 0
                    : _dx == 0
                    ;

            if (dx > 0 && _dx <= 0 || dy > 0 && _dy <= 0) {
                for (RSScrollStatusObserver observer : scrollStatusObservers) {
                    observer.onStartScrollDirection(
                            RSBaseRecyclerView.this,
                            true,
                            fromIdle
                    );
                }
            }

            if (_dx >= 0 && dx < 0 || _dy >= 0 && dy < 0) {
                for (RSScrollStatusObserver observer : scrollStatusObservers) {
                    observer.onStartScrollDirection(
                            RSBaseRecyclerView.this,
                            false,
                            fromIdle
                    );
                }
            }

            _dx = dx;
            _dy = dy;

            int visibleItemCount = theManager.getChildCount();
            int totalItemCount   = theManager.getItemCount();

            int firstVisibleItemPosition = theManager.findFirstVisibleItemPosition();
            int lastVisibleItemPosition  = theManager.findLastVisibleItemPosition();

            int bottomPosition = totalItemCount - 1;

            boolean lastOnTop    = lastHeadPosition == 0;
            boolean lastOnBottom = lastTailPosition == bottomPosition;
            boolean currentOnTop    = firstVisibleItemPosition == 0;
            boolean currentOnBottom = lastVisibleItemPosition  == bottomPosition;

            lastHeadPosition = firstVisibleItemPosition;
            lastTailPosition = lastVisibleItemPosition;

            if (dx == 0 && dy == 0) {
                return;
            }

            if (visibleItemCount < totalItemCount) {
                // Touch bottom event.
                if (!lastOnBottom && currentOnBottom) {
                    for (RSScrollStatusObserver observer : scrollStatusObservers) {
                        observer.onTouchBottom(RSBaseRecyclerView.this);
                    }
                }

                // Touch top event.
                if (!lastOnTop && currentOnTop) {
                    for (RSScrollStatusObserver observer : scrollStatusObservers) {
                        observer.onTouchTop(RSBaseRecyclerView.this);
                    }
                }

                // Leave bottom event.
                if (lastOnBottom && !currentOnBottom) {
                    for (RSScrollStatusObserver observer : scrollStatusObservers) {
                        observer.onLeaveBottom(RSBaseRecyclerView.this);
                    }
                }

                // Leave top event.
                if (lastOnTop && !currentOnTop) {
                    for (RSScrollStatusObserver observer : scrollStatusObservers) {
                        observer.onLeaveTop(RSBaseRecyclerView.this);
                    }
                }
            }
        }
    }

    public interface RSScrollStatusObserver {
        /**
         * On turn over.
         *
         * @param turnToAscent  Direction changed
         * @param fromIdle  Whether the change from the stationary state
         */
        void onStartScrollDirection(
                RSBaseRecyclerView recyclerView,
                boolean turnToAscent,
                boolean fromIdle
        );

        /**
         * Touch bottom event.
         */
        void onTouchBottom(RSBaseRecyclerView recyclerView);

        /**
         * Touch top event.
         */
        void onTouchTop(RSBaseRecyclerView recyclerView);

        /**
         * Leave bottom event.
         */
        void onLeaveBottom(RSBaseRecyclerView recyclerView);

        /**
         * Leave top event.
         */
        void onLeaveTop(RSBaseRecyclerView recyclerView);
    }
}
