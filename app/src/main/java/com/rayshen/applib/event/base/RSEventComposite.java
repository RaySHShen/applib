package com.rayshen.applib.event.base;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Shu-Hua Shen on 2017/5/2.
 */
public class RSEventComposite extends RSEventBase {

    private CompositeDisposable    compositeDisposable;
    private Object                 object;
    private Set<RSEventSubscriber> subscriberEvents;

    public RSEventComposite(CompositeDisposable compositeDisposable, Object object, Set<RSEventSubscriber> subscriberEvents) {
        this.compositeDisposable = compositeDisposable;
        this.object = object;
        this.subscriberEvents = subscriberEvents;
    }

    public final CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    public final void setCompositeDisposable(CompositeDisposable compositeDisposable) {
        this.compositeDisposable = compositeDisposable;
    }

    public final Object getObject() {
        return object;
    }

    public final void setObject(Class<?> object) {
        this.object = object;
    }

    public final Set<RSEventSubscriber> getSubscriberEvents() {
        return subscriberEvents;
    }

    public final void setSubscriberEvents(Set<RSEventSubscriber> subscriberEvents) {
        this.subscriberEvents = subscriberEvents;
    }

    public final void subscriberSticky(Map<Class<?>, Object> objectMap) {
        List<Class> classes = new ArrayList<>();
        for (Map.Entry<Class<?>, Object> classObjectEntry : objectMap.entrySet()) {
            for (RSEventSubscriber subscriberEvent : subscriberEvents) {
                if (classObjectEntry.getKey() == subscriberEvent.getParameter()) {
                    try {
                        classes.add(classObjectEntry.getKey());
                        subscriberEvent.handleEvent(classObjectEntry.getValue());
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        stickyEventMapRemove(classes);
    }
}
