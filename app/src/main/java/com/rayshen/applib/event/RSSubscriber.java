package com.rayshen.applib.event;

import com.rayshen.applib.event.base.ThreadMode;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Shu-Hua Shen on 2017/5/12.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RSSubscriber {
    ThreadMode threadMode() default ThreadMode.MAIN_THREAD;
}
