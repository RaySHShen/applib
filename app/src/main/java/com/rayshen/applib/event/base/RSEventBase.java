package com.rayshen.applib.event.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by Shu-Hua Shen on 2017/5/2.
 */
public class RSEventBase {

    protected final static Map<Class<?>, Object> sStickEventMap;
    protected final static Subject<Object>       sSubject;

    static {
        sSubject = PublishSubject.create().toSerialized();
        sStickEventMap = new HashMap<>();
    }

    protected RSEventBase() {
    }

    static <T> Flowable<T> toFlowable(Class<T> eventType) {
        return sSubject.ofType(eventType).toFlowable(BackpressureStrategy.BUFFER);
    }

    static synchronized void dellSticky(Object event) {
        if (!sStickEventMap.isEmpty()) {
            List<Class> classes = new ArrayList<>();
            for (Map.Entry<Class<?>, Object> objectEntry : sStickEventMap.entrySet()) {
                if (objectEntry.getKey() == event.getClass()) {
                    classes.add(event.getClass());
                }
            }
            stickyEventMapRemove(classes);
        }
    }

    static void stickyEventMapRemove(List<Class> classes) {
        for (Class aClass : classes) sStickEventMap.remove(aClass);
    }
}
