package com.rayshen.applib.event;

/**
 * Created by Shu-Hua Shen on 2017/5/12.
 */
public interface IBus
{
    void register(Object object);
    void unregister(Object object);
    void post(IEvent event);
    void postSticky(IEvent event);
}
