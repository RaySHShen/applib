package com.rayshen.applib.event;

import com.rayshen.applib.event.base.RSEventBase;
import com.rayshen.applib.event.base.RSEventComposite;
import com.rayshen.applib.event.base.RSEventFind;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Shu-Hua Shen on 2017/5/12.
 */
public class RxBusImpl extends RSEventBase implements IBus
{

    private ConcurrentMap<Object, RSEventComposite> mEventCompositeMap = new ConcurrentHashMap<>();

    @Override
    public void register(Object object) {
        if (object == null) {
            throw new NullPointerException("Object to register must not be null.");
        }
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        RSEventComposite    subscriberMethods   = RSEventFind.findAnnotatedSubscriberMethods(object, compositeDisposable);
        mEventCompositeMap.put(object, subscriberMethods);

        if (!sStickEventMap.isEmpty()) {
            subscriberMethods.subscriberSticky(sStickEventMap);
        }
    }

    @Override
    public void unregister(Object object) {
        if (object == null) {
            throw new NullPointerException("Object to register must not be null.");
        }
        RSEventComposite subscriberMethods = mEventCompositeMap.get(object);
        if (subscriberMethods != null) {
            subscriberMethods.getCompositeDisposable().dispose();
        }
        mEventCompositeMap.remove(object);
    }

    @Override
    public void post(IEvent event) {
        sSubject.onNext(event);
    }

    @Override
    public void postSticky(IEvent event) {
        sStickEventMap.put(event.getClass(), event);
        post(event);
    }
}
