package com.rayshen.applib.event.base;

import com.rayshen.applib.event.RSSubscriber;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Shu-Hua Shen on 2017/5/2.
 */
public class RSEventFind {

    public static RSEventComposite findAnnotatedSubscriberMethods(Object listenerClass, CompositeDisposable compositeDisposable) {
        Set<RSEventSubscriber> producerMethods = new HashSet<>();
        return findAnnotatedMethods(listenerClass, producerMethods, compositeDisposable);
    }

    private static RSEventComposite findAnnotatedMethods(Object listenerClass, Set<RSEventSubscriber> subscriberMethods,
            CompositeDisposable compositeDisposable) {
        for (Method method : listenerClass.getClass().getDeclaredMethods()) {
            if (method.isBridge()) {
                continue;
            }
            if (method.isAnnotationPresent(RSSubscriber.class)) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length != 1) {
                    throw new IllegalArgumentException("Method " + method + " has @Subscribe annotation but requires " + parameterTypes
                            .length + " arguments.  Methods must require a single argument.");
                }

                Class<?> parameterClazz = parameterTypes[0];
                if ((method.getModifiers() & Modifier.PUBLIC) == 0) {
                    throw new IllegalArgumentException("Method " + method + " has @EventSubscribe annotation on " + parameterClazz + " " +
                                                       "but is not 'public'.");
                }

                RSSubscriber annotation = method.getAnnotation(RSSubscriber.class);
                ThreadMode thread = annotation.threadMode();

                RSEventSubscriber subscriberEvent = new RSEventSubscriber(listenerClass, method, thread);
                if (!subscriberMethods.contains(subscriberEvent)) {
                    subscriberMethods.add(subscriberEvent);
                    compositeDisposable.add(subscriberEvent.getDisposable());
                }
            }
        }
        return new RSEventComposite(compositeDisposable, listenerClass, subscriberMethods);
    }
}
